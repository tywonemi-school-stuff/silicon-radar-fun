\section{Radar fundamentals}
Measuring distances and velocities of objects is crucial for controlling complex systems. With good reason does the mammal brain use an adaptively trained mix of heuristics and approximations to this end\cite{Howard2012}. We could consider the earliest attempts at converting distance measurements, a typically empirical problem solved with a long piece of string, to a more scalable one through theory, to be t hose of Eratosthenes who trigonometrically computed the diameter of the Earth with an error of 1.4\% using a short stick\cite{Eratosthenes265BC}. Since the third century BC, humanity has progressed somewhat, and has uncovered a handful of reliable laws, such as that of a finite speed of light\cite{Einstein1905}, later generalized to a finite speed of information propagation\cite{milonni2005fast}. This has taught us that we could measure distances from reflective objects if we could send a pulse of light, and measure its time of flight. Due to implementation difficulties beyond the scope of this work, this technique, named lidar, was developed only as a secondary tool derived from the radio frequency equivalent, radar, which uses the fact that light is a special case of electromagnetic radiation\cite{Maxwell1865}.\par
\section{Frequency accuracy}
We also have at our disposal incredibly precise methods of measuring frequencies and time intervals, using atom oscillations\cite{ESSEN1955}, to which we have affixed our very notion of time. More affordably and reliably, rather than every device being equipped a caesium oscillator, we can use off-the-shelf crystals with frequency errors under 10 parts-per-million, receive radio-distributed accurate time signaling, provided by Global Navigation Satellite Systems, lock onto these with a phase-locked loop either in hardware on software, calibrate the crystal, and eliminate the temperature-caused shorter-term frequency drift with some polynomial compensation. This gives us scalable, accurate frequency measurement on small devices.
\section{Field-programmable gate array (FPGA) fundamentals}
To understand the allure of FPGAs, we must first remind ourselves of the shortcomings of processors. A processor is a device which executes instructions from memory. It operates on them sequentially, a small number of operations at a time. It is a finite state machine, but from the perspective of a human, the number of possible states is infinite, it is very hard to guarantee any sort of latency. If one manages to guarantee some latency in software, it is always at the cost of mean latency, and performance loss. In short, processors are great at executing arbitrary algorithms in a sufficiently small number of clock cycles. \par
On the other hand, FPGAs behave exactly like hard-wired logic. Arbitrary digital systems can be built out of their configurable logic blocks, which are connected by a "fabric", a fancy name for a complex, undocumented crossbar-style digital multiplexer. They can be used for designs either asynchronous or synchronous to internal clocks and typically include 
\begin{itemize}
    \item fast distributed RAM
    \item large block RAM
    \item digital look-up tables for implementing arbitrary logical functions with efficient silicon area usage
    \item digital signal processing blocks for arithmetics
    \item digital input-output interfaces to interface with the world with high speed protocols
\end{itemize}
It is very common to combine the best of both approaches and synthesize simple processor cores on FPGAs, since processors fully consist from logic gates and some memory. Also, Systems on Chip (SoC), that integrate processor cores and FPGA fabrics are becoming popular for applications such as metrology or motor control. \par
However, at the time of writing, implementing algorithms in FPGAs is limited to niche applications. Deterministic latency is required in critical systems in aerospace\cite{7231150}, defense\cite{6154185} and medical\cite{8325876} applications, as well as high end media production equipment\cite{8966063}. Some of the reasons for this are for example the lower complexity of developing embedded hardware with microcontrollers, and where such generic silicon lacks in performance or latency, application specific integrated circuits and application specific processors are more efficiently manufactured at scale, and can be tuned more finely for low power consumption and clock speeds.
\vfill
\section{Microcontroller fundamentals}
Microcontrollers are simple integrated circuits with a processor core or multiple cores, on-board memory both volatile and non-volatile, and a set of peripherals typically mapped onto a bus or multiple. For simpler embedded devices, performance is a lower priority over reliability. For this reason, microcontrollers are most commonly programmed without any sort of operating system. If some sort of multi-threading is desired, real time operating systems (RTOS) which, unlike personal computer oriented systems such as linux, provide simpler means of switching contexts, for the sake of small code size and determinism. Peripherals often found on microcontrollers include timers, direct memory access (DMA) controllers, serial communication interfaces such as SPI and I2C, mixed signal blocks such as analog-to-digital and digital-to-analog converters, and debug functionality for loading and debugging firmware.
\section{Range and velocity target detection}
This section will use the formalism of Bandiera, Coluccia, Dodde, Masciullo, Ricci 2016\cite{IQcalCRLB} with modifications for more explicit assumptions and description of multi-chirp operation.\par
The first chirp from a FMCW radar, such as that in our system, is transmitted as a frequency ramp:
\begin{align}
    f(t) &= f_0 + \alpha t,\, 0 \leq t < T
\end{align} 
$f_0$ being the carrier frequency, $t$ time, $T$ the duration of the chirp, $T_c$ the time between chirp starts, $\alpha$ the chirp "rate".
Let us enumerate chirps with $k$ up to a chirp count of $M$:
\begin{align}
    f(t) &= f_0 + \alpha \left( t - \left(k - 1\right)  T_c \right) ,\,\\
     (k - 1)T_c& \leq t < (k - 1)T_c + T
\end{align} 
Let us call the time since the start of chirp k $t_k = t - (k-1)T_c$.
The phase of the first chirp is simply frequency integrated over time:
\begin{align}
    \phi(t)&=2 \pi \int_{0}^{t} f(\tau) d \tau=2 \pi f_{0} t+\pi \alpha t^{2}
\end{align}
For chirp $k$, in a voltage-controlled oscillator or digital equivalents, the phase is continuous, even at a jump in frequency, hence:
\begin{align}
    \phi[k](t) &=2 \pi \int_{0}^{t} f(\tau) d \tau=2 \pi f_{0} t_k +\pi \alpha t_k^{2} + k\phi(T)
\end{align}
For clarity, let us ignore $k\phi(T)$, arbitrary phase offsets in chirps eliminate themselves later either way. The radar generates in its local oscillator a real-valued signal, which is radiated by the TX antenna:
\begin{align}
    l[k](t)&=A_{L} \cos (\phi[k](t))
\end{align}
Assuming that targets are moving at constant speeds, the distance $r$ and time of signal flight $\tau$ can be expressed:
\begin{align}
    r(t)&=r_{0}+v t = r_{0} + vkT_c + vt_k\\
    \tau(t) &= \frac{2r(t)}{c(1+\frac{v}{c})} \approx \frac{2r(t)}{c}
    \label{eq:delay}
\end{align}
where $c$ is the speed of light. We assume the targets's speed to be significantly slower than the speed of light from this step onward. The reflected signal is the delayed radiated signal, attenuated:
\begin{align}
    s[k](t)=A_{R} \cos \left(\phi[k]\left(t-\tau(t)\right)\right)+w(t)
\end{align}
where $w(t)$ is AWGN. 
This is downmixed with the local oscillator signal, and the local oscillator signal phase-shifted by $\frac{\pi}{2}$, to generate the I/Q signals. At this point, inaccuracy is introduced in the form of a gain error $g_e$ between the I and Q signal, as well as an error in the phase shift $\phi_e$ of the local oscillator. The local oscillator I/Q signals have the following forms in each chirp:
\begin{align}
    l_I[k](t) &= \frac{A_L}{\sqrt{2}}\cos(\phi(t))\\
    l_Q[k](t) &= g_e\frac{A_L}{\sqrt{2}}\sin(\phi(t) + \phi_e)
\end{align}
Downmixing consists in multiplication of (assumed harmonic) signals in time-domain, and low-pass filtering all resulting terms at frequencies other than that of the difference term. Assuming an ideal low-pass, after this mix we arrive at the following "baseband" signal pair:
\begin{align}
    b_I[k](t) &= 2a^\prime\cos(2\pi f_T t_k + \theta) + n_I(t)\\
    b_Q[k](t) &= 2a^\prime g_e\sin(2\pi f_T t_k + \theta + \phi_e) + n_Q(t)
\end{align}
where $f_{T}=2 \alpha \frac{r_{0}}{c}, \theta = 4\pi\frac{f_0 (r_0 + vkT_c)}{c}, a^{\prime}=\frac{A_{L} A_{R}}{4 \sqrt{2}}$ and $n_I(t)$, $n_Q(t)$ are possibly correlated low-pass filtered white Gaussian noise, but assumed uncorrelated zero-mean for the purposes of I/Q calibration. $f_T$ also has a Doppler-effect element of $2\frac{v}{c}f_0$, not expressed above, however, with our FMCW parameters, this turns out to be smaller than the distance-delay element of $2\alpha\frac{r_0}{c}$ by a factor of $10^{-3}$. We are going to be extracting the velocity data by using the difference in range over our multiple chirps instead. At this point, let's assume we are receiving a perfect baseband signal, that is, $g_e = 1, \phi_e = 1$. We can now see that the baseband signal pair consists of a real and imaginary part of a complex-valued signal:
\begin{align}
    \hat{b}[k](t) &= 2a^\prime\exp(2\pi \mj f_T t_k + \theta)
\end{align}
Now we sample the baseband signal pair at a sufficiently high frequency of $f_s$, the sampling period being $T_s$. Afterwards, if we perform a discrete Fourier transform (DFT) of each chirp along the sampled chirp time ($t_k$) axis, we can detect a peak at $f_T$. Let's hide uninteresting multiplicative constants into $a^{\prime\prime}$ and for simplicity's sake perform a continuous Fourier transform of one period:
\begin{align}
    \hat{b}[k](\omega) &= a^{\prime\prime}exp\left(\mj k \left(4\pi f_0 T_c \frac{v}{c}\right)\right)\dirac\left(\omega-2\alpha\frac{r_0 + kT_c v}{c}\right)
\end{align}
If we now perform a DFT along the chirp index axis:
\begin{align}
    \hat{b}[\Omega^\prime](\omega) &= a^{\prime\prime}\dirac\left(\Omega^\prime - \left(4\pi f_0 T_c \frac{v}{c}\right)\right)\dirac\left(\omega-2\alpha\frac{r_0 + kT_c v}{c}\right)
\end{align}
periodic with regards to $\Omega^\prime$. That being said, in implementation, the untransformed signal is discrete along both axes; furthermore, the FFT only gives us a single period of the true DFT. With trivial manipulations, we find that we can unambiguously detect targets at distances of up to $r_{max} = \frac{c f_s}{4 \alpha}$ and velocities of up to $v_{max} = \frac{c f_0}{4 T_c}$. The accuracy of $r$,$v$, from DFT characteristics, becomes $\Delta r = \frac{2r_{max}}{f_s T}$, $\Delta v = \frac{2v_{max}}{M}$. We can see, that theoretically, if we let the chirp count and sampling frequency approach infinity, we can perfectly resolve distances and velocities of arbitrarily far-away objects moving at constant subluminal velocities, which fits our intuitions of "the better the hardware and the more information, the better the resulting data". Curiously, this seems to violate the Fourier-transform logic behind the Heisenberg uncertainty principle; however, we are not using a single measurement here, but an infinite series of measurements.\vfill
\section{The USB Protocol}
The USB protocol\cite{usbspec2} provides guaranteed in-order delivery between a bus host and a connected device. An endpoint is a virtual construct, the functions of which are typically implemented in hardware to various degrees. The STM32 USB peripherals, for example, treat endpoints as configurable FIFOs where packets are stored and sent or received by the USB core logic, without involvement of the processor core, triggering interrupts for the processor to come pick up its received data or queue more data for transmission. Personal computer peripherals treat them similarly, but an application does not have to work directly with FIFOs, since such operations are handled by the operating systems in privileged modes by kernel modules (DMA) and drivers. In USB protocol terminology, raw data flows through "pipes", which are virtual connections between two endpoints. The developer uses this terminology, opens a pipe and request data on it without worrying about packet structure. There are two types of pipes: unidirectional "stream pipes" directed by either the host or device, and bidirectional host-directed "message pipes". The directions are called, from the perspective of the bus host, IN and OUT.\par 
The protocol has multiple transfer types, which occur between endpoints. Bidirectional endpoint 0 is reserved for control transfers, which convey the state of the bus, including data requests, over message pipes. The transfers which use stream pipes to convey data are interrupt, isochronous, and bulk transfers. Interrupt transfers are non-periodic and handled with higher priority than the rest, isochronous transfers are periodic and get assigned a special portion at the start of USB data frames, and bulk transfers are designated for large amounts of data. In reality, all but control and bulk are considered deprecated. Isochronous transfers seem to have been designed with extremely simple devices with application-specific integrated circuit controllers, and have not been designed to scale well with bandwidth increases such as USB microframes. Isochronous transfers have no error detection. Bulk transfers have the limitation of being 8, 16, 32, or 64 bytes on USB-FS devices, 256 or 512 on USB-HS, and up to 1024 bytes in USB-SS.\par
When a device is connected to a host, the presence of the device is detected as a load on the 5V VBUS. For this reason, the capacitance of a USB device at the time of connection is limited to a range of \SI{1}{\micro\farad} to \SI{10}{\micro\farad}. It is not clear from the standard whether capacitively loading the VBUS line further after connection is acceptable.\par
The USB protocol provides multiple data speeds. Most ubiqutously implemented in desktop peripherals is Full Speed, which provides 12Mbps and has rather lenient accuracy requirements. USB 2.0 adds High Speed at 480Mbps, a bandwidth sufficient for carrying live video.
\pagebreak
\section{Future of radar}
Among the most notable directions the development of radar technology has been taking is certainly accessibility. Using modern MMIC (microwave integrated circuit) production capabilities, the cost of designing and bringing to market a device with an oscillator, rat-race and mixer has declined. Passenger cars are starting to use radars as a part of their sensor suite for collision avoidance, paving the road for full autonomy. Medical uses of radar have been proposed, such as breathing rhythm detection. \par
Significant advances have been made through advanced digital signal processing work on beam-steering and beam-shaping, in conjunction with the development of modern RF communication standards. Increases in resolution beyond the traditional wavelength limitation have been achieved. MIMO (multiple input, multiple output) systems operate at the ever-increasing limits of embedded computing, using mathematical tricks to reduce ambiguity and inaccuracy, at the cost of computational copmlexity. \par
Last but not least, there is an increasing potential of applying machine learning to object detection. Detecting and classifying objects in video feeds is a highly popular and productive field of study, greatly surpassing classical algorithms in adaptability and increasingly also in low computational complexity once trained. 
% \section{Bats}
% they are v v cool