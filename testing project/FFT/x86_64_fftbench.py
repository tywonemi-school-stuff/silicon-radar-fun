import numpy.fft
import pyfftw
import time

import numpy as np

N = 1024
dtype = np.complex
repeat = 1000000

a = numpy.random.random(N) + complex(0, 1)*numpy.random.random(N)
a = a.astype(dtype)

pyfftw.config.NUM_THREADS = 1

fftw_in = np.zeros(N, dtype=dtype)
fftw_out = np.zeros(N, dtype=dtype)
fftw = pyfftw.FFTW(fftw_in, fftw_out, flags=["FFTW_PATIENT"])

start = time.process_time()
for i in range(repeat):
    np.copyto(fftw_in, a)
    fftw.execute()
end = time.process_time()
print("FFTW took  " + str((end - start)/repeat))

numpy.fft.fft(a)
exit()
start = time.process_time()
for i in range(repeat):
    c = numpy.fft.fft(a)
end = time.process_time()

print("Numpy took " + str((end - start)/repeat))