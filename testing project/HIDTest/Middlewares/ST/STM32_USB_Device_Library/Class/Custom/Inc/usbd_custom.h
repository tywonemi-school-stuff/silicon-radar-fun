
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_CUSTOM_H
#define __USB_CUSTOM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include  "usbd_ioreq.h"

/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */

/** @defgroup USBD_Custom
  * @brief This file is the Header file for usbd_Custom.c
  * @{
  */


/** @defgroup USBD_Custom_Exported_Defines
  * @{
  */
#define Custom_fat_EPIN_ADDR                 0x81U
#define Custom_fat_EPIN_SIZE                 0x10U
#define Custom_config_EPOUT_ADDR              0x02U
#define Custom_config_EPOUT_SIZE              0x04U
#define Custom_debug_EPIN_ADDR              0x83U
#define Custom_debug_EPIN_SIZE              0x04U

#define USB_Custom_CONFIG_DESC_SIZ       39U
#define USB_Custom_DESC_SIZ              9U
#define Custom_MOUSE_REPORT_DESC_SIZE    74U

#define Custom_DESCRIPTOR_TYPE           0x21U
#define Custom_REPORT_DESC               0x22U

#ifndef Custom_HS_BINTERVAL
#define Custom_HS_BINTERVAL            0x07U
#endif /* Custom_HS_BINTERVAL */

#ifndef Custom_FS_BINTERVAL
#define Custom_FS_BINTERVAL            0x0AU
#endif /* Custom_FS_BINTERVAL */

#define Custom_REQ_SET_PROTOCOL          0x0BU
#define Custom_REQ_GET_PROTOCOL          0x03U

#define Custom_REQ_SET_IDLE              0x0AU
#define Custom_REQ_GET_IDLE              0x02U

#define Custom_REQ_SET_REPORT            0x09U
#define Custom_REQ_GET_REPORT            0x01U
/**
  * @}
  */


/** @defgroup USBD_CORE_Exported_TypesDefinitions
  * @{
  */
typedef enum
{
  Custom_IDLE = 0,
  Custom_BUSY,
}
Custom_StateTypeDef;


typedef struct
{
  uint32_t             Protocol;
  uint32_t             IdleState;
  uint32_t             AltSetting;
  Custom_StateTypeDef     state;
  uint8_t  *RxBuffer;
  uint8_t  *TxBuffer;
  uint32_t RxLength;
  uint32_t TxLength;

  __IO uint32_t TxState;
  __IO uint32_t RxState;
}
USBD_Custom_HandleTypeDef;
/**
  * @}
  */



/** @defgroup USBD_CORE_Exported_Macros
  * @{
  */

/**
  * @}
  */

/** @defgroup USBD_CORE_Exported_Variables
  * @{
  */

extern USBD_ClassTypeDef  USBD_Custom;
#define USBD_Custom_CLASS    &USBD_Custom
/**
  * @}
  */

/** @defgroup USB_CORE_Exported_Functions
  * @{
  */
uint8_t USBD_Custom_SendReport(USBD_HandleTypeDef *pdev,
                            uint8_t *report,
                            uint16_t len);

uint32_t USBD_Custom_GetPollingInterval(USBD_HandleTypeDef *pdev);

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif  /* __USB_Custom_H */
/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
