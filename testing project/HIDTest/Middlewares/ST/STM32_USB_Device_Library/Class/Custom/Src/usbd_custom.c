/* BSPDependencies
- "stm32xxxxx_{eval}{discovery}{nucleo_144}.c"
- "stm32xxxxx_{eval}{discovery}_io.c"
EndBSPDependencies */

/* Includes ------------------------------------------------------------------*/
#include "usbd_Custom.h"
#include "usbd_ctlreq.h"


/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */


/** @defgroup USBD_Custom
  * @brief usbd core module
  * @{
  */

/** @defgroup USBD_Custom_Private_TypesDefinitions
  * @{
  */
/**
  * @}
  */


/** @defgroup USBD_Custom_Private_Defines
  * @{
  */

/**
  * @}
  */


/** @defgroup USBD_Custom_Private_Macros
  * @{
  */
/**
  * @}
  */




/** @defgroup USBD_Custom_Private_FunctionPrototypes
  * @{
  */


static uint8_t  USBD_Custom_Init(USBD_HandleTypeDef *pdev,
                              uint8_t cfgidx);

static uint8_t  USBD_Custom_DeInit(USBD_HandleTypeDef *pdev,
                                uint8_t cfgidx);

static uint8_t  USBD_Custom_Setup(USBD_HandleTypeDef *pdev,
                               USBD_SetupReqTypedef *req);

static uint8_t  *USBD_Custom_GetFSCfgDesc(uint16_t *length);

static uint8_t  *USBD_Custom_GetHSCfgDesc(uint16_t *length);

static uint8_t  *USBD_Custom_GetOtherSpeedCfgDesc(uint16_t *length);

static uint8_t  *USBD_Custom_GetDeviceQualifierDesc(uint16_t *length);

static uint8_t  USBD_Custom_DataIn(USBD_HandleTypeDef *pdev, uint8_t epnum);

static uint8_t  USBD_Custom_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum);

/**
  * @}
  */

/** @defgroup USBD_Custom_Private_Variables
  * @{
  */

USBD_ClassTypeDef  USBD_Custom =
{
  USBD_Custom_Init,
  USBD_Custom_DeInit,
  USBD_Custom_Setup,
  NULL, /*EP0_TxSent*/
  NULL, /*EP0_RxReady*/
  USBD_Custom_DataIn, /*DataIn*/
  USBD_Custom_DataOut, /*DataOut*/
  NULL, /*SOF */
  NULL,
  NULL,
  USBD_Custom_GetHSCfgDesc,
  USBD_Custom_GetFSCfgDesc,
  USBD_Custom_GetOtherSpeedCfgDesc,
  USBD_Custom_GetDeviceQualifierDesc,
};

/* USB Custom device FS Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_Custom_CfgFSDesc[USB_Custom_CONFIG_DESC_SIZ]  __ALIGN_END =
{
  0x09, /* bLength: Configuration Descriptor size */
  USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
  USB_Custom_CONFIG_DESC_SIZ,  /* wTotalLength: Bytes returned */
  0x00,         /* wTotalLength higher byte*/
  0x01,         /*bNumInterfaces: 1 interface*/
  0x01,         /*bConfigurationValue: Configuration value*/
  0x00,         /*iConfiguration: Index of string descriptor describing
  the configuration*/
  0xE0,         /*bmAttributes: bus powered and Support Remote Wake-up */
  0x32,         /*MaxPower 100 mA: this current is used for detecting Vbus*/

  /************** Descriptor of Joystick Mouse interface ****************/
  /* 09 */
  0x09,         /*bLength: Interface Descriptor size*/
  USB_DESC_TYPE_INTERFACE,/*bDescriptorType: Interface descriptor type*/
  0x00,         /*bInterfaceNumber: Number of Interface*/
  0x00,         /*bAlternateSetting: Alternate setting*/
  0x03,         /*bNumEndpoints*/
  0xFF,         /*bInterfaceClass: Custom*/
  0xFF,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
  0xFF,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
  0,            /*iInterface: Index of string descriptor*/
  /******************** Descriptor of Joystick Mouse Custom ********************/
  // /* 18 */
  // 0x09,         /*bLength: Custom Descriptor size*/
  // Custom_HID_DESCRIPTOR_TYPE, /*bDescriptorType: Custom*/
  // 0x11,         /*bcdCustom: Custom Class Spec release number*/
  // 0x01,
  // 0x00,         /*bCountryCode: Hardware target country*/
  // 0x01,         /*bNumDescriptors: Number of Custom class descriptors to follow*/
  // 0x22,         /*bDescriptorType*/
  // Custom_MOUSE_REPORT_DESC_SIZE,/*wItemLength: Total length of Report descriptor*/
  // 0x00,
  /******************** Descriptor of Mouse endpoint ********************/
  /* 18 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/

  Custom_fat_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x02,          /*bmAttributes: Bulk endpoint*/
  Custom_fat_EPIN_SIZE, /*wMaxPacketSize: in Bytes */
  0x00,
  Custom_FS_BINTERVAL,          /*bInterval: Polling Interval */
  
  
  /* 25 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/

  Custom_config_EPOUT_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x02,          /*bmAttributes: Bulk endpoint*/
  Custom_config_EPOUT_SIZE, /*wMaxPacketSize: in Bytes */
  0x00,
  Custom_FS_BINTERVAL,          /*bInterval: Polling Interval */
  
  
  /* 32 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/

  Custom_debug_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x02,          /*bmAttributes: Bulk endpoint*/
  Custom_debug_EPIN_SIZE, /*wMaxPacketSize: in Bytes */
  0x00,
  Custom_FS_BINTERVAL,          /*bInterval: Polling Interval */
  /* 39 */
};

/* USB Custom device HS Configuration Descriptor */
// __ALIGN_BEGIN static uint8_t USBD_Custom_CfgHSDesc[USB_Custom_CONFIG_DESC_SIZ]  __ALIGN_END =
// {
//   0x09, /* bLength: Configuration Descriptor size */
//   USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
//   USB_Custom_CONFIG_DESC_SIZ,
//   /* wTotalLength: Bytes returned */
//   0x00,
//   0x01,         /*bNumInterfaces: 1 interface*/
//   0x01,         /*bConfigurationValue: Configuration value*/
//   0x00,         /*iConfiguration: Index of string descriptor describing
//   the configuration*/
//   0xE0,         /*bmAttributes: bus powered and Support Remote Wake-up */
//   0x32,         /*MaxPower 100 mA: this current is used for detecting Vbus*/

//   /************** Descriptor of Joystick Mouse interface ****************/
//   /* 09 */
//   0x09,         /*bLength: Interface Descriptor size*/
//   USB_DESC_TYPE_INTERFACE,/*bDescriptorType: Interface descriptor type*/
//   0x00,         /*bInterfaceNumber: Number of Interface*/
//   0x00,         /*bAlternateSetting: Alternate setting*/
//   0x01,         /*bNumEndpoints*/
//   0x03,         /*bInterfaceClass: Custom*/
//   0x01,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
//   0x02,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
//   0,            /*iInterface: Index of string descriptor*/
//   /******************** Descriptor of Joystick Mouse Custom ********************/
//   /* 18 */
//   0x09,         /*bLength: Custom Descriptor size*/
//   Custom_DESCRIPTOR_TYPE, /*bDescriptorType: Custom*/
//   0x11,         /*bcdCustom: Custom Class Spec release number*/
//   0x01,
//   0x00,         /*bCountryCode: Hardware target country*/
//   0x01,         /*bNumDescriptors: Number of Custom class descriptors to follow*/
//   0x22,         /*bDescriptorType*/
//   Custom_MOUSE_REPORT_DESC_SIZE,/*wItemLength: Total length of Report descriptor*/
//   0x00,
//   /******************** Descriptor of Mouse endpoint ********************/
//   /* 27 */
//   0x07,          /*bLength: Endpoint Descriptor size*/
//   USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/

//   Custom_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
//   0x03,          /*bmAttributes: Interrupt endpoint*/
//   Custom_EPIN_SIZE, /*wMaxPacketSize: 4 Byte max */
//   0x00,
//   Custom_HS_BINTERVAL,          /*bInterval: Polling Interval */
//   /* 34 */
// };

// /* USB Custom device Other Speed Configuration Descriptor */
// __ALIGN_BEGIN static uint8_t USBD_Custom_OtherSpeedCfgDesc[USB_Custom_CONFIG_DESC_SIZ]  __ALIGN_END =
// {
//   0x09, /* bLength: Configuration Descriptor size */
//   USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
//   USB_Custom_CONFIG_DESC_SIZ,
//   /* wTotalLength: Bytes returned */
//   0x00,
//   0x01,         /*bNumInterfaces: 1 interface*/
//   0x01,         /*bConfigurationValue: Configuration value*/
//   0x00,         /*iConfiguration: Index of string descriptor describing
//   the configuration*/
//   0xE0,         /*bmAttributes: bus powered and Support Remote Wake-up */
//   0x32,         /*MaxPower 100 mA: this current is used for detecting Vbus*/

//   /************** Descriptor of Joystick Mouse interface ****************/
//   /* 09 */
//   0x09,         /*bLength: Interface Descriptor size*/
//   USB_DESC_TYPE_INTERFACE,/*bDescriptorType: Interface descriptor type*/
//   0x00,         /*bInterfaceNumber: Number of Interface*/
//   0x00,         /*bAlternateSetting: Alternate setting*/
//   0x01,         /*bNumEndpoints*/
//   0x03,         /*bInterfaceClass: Custom*/
//   0x01,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
//   0x02,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
//   0,            /*iInterface: Index of string descriptor*/
//   /******************** Descriptor of Joystick Mouse Custom ********************/
//   /* 18 */
//   0x09,         /*bLength: Custom Descriptor size*/
//   Custom_DESCRIPTOR_TYPE, /*bDescriptorType: Custom*/
//   0x11,         /*bcdCustom: Custom Class Spec release number*/
//   0x01,
//   0x00,         /*bCountryCode: Hardware target country*/
//   0x01,         /*bNumDescriptors: Number of Custom class descriptors to follow*/
//   0x22,         /*bDescriptorType*/
//   Custom_MOUSE_REPORT_DESC_SIZE,/*wItemLength: Total length of Report descriptor*/
//   0x00,
//   /******************** Descriptor of Mouse endpoint ********************/
//   /* 27 */
//   0x07,          /*bLength: Endpoint Descriptor size*/
//   USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/

//   Custom_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
//   0x03,          /*bmAttributes: Interrupt endpoint*/
//   Custom_EPIN_SIZE, /*wMaxPacketSize: 4 Byte max */
//   0x00,
//   Custom_FS_BINTERVAL,          /*bInterval: Polling Interval */
//   /* 34 */
// };


/* USB Custom device Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_Custom_Desc[USB_Custom_DESC_SIZ]  __ALIGN_END  =
{
  /* 18 */
  0x09,         /*bLength: Custom Descriptor size*/
  Custom_DESCRIPTOR_TYPE, /*bDescriptorType: Custom*/
  0x11,         /*bcdCustom: Custom Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of Custom class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  Custom_MOUSE_REPORT_DESC_SIZE,/*wItemLength: Total length of Report descriptor*/
  0x00,
};

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_Custom_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC]  __ALIGN_END =
{
  USB_LEN_DEV_QUALIFIER_DESC,
  USB_DESC_TYPE_DEVICE_QUALIFIER,
  0x00,
  0x02,
  0x00,
  0x00,
  0x00,
  0x40,
  0x01,
  0x00,
};

__ALIGN_BEGIN static uint8_t Custom_MOUSE_ReportDesc[Custom_MOUSE_REPORT_DESC_SIZE]  __ALIGN_END =
{
  0x05,   0x01,
  0x09,   0x02,
  0xA1,   0x01,
  0x09,   0x01,

  0xA1,   0x00,
  0x05,   0x09,
  0x19,   0x01,
  0x29,   0x03,

  0x15,   0x00,
  0x25,   0x01,
  0x95,   0x03,
  0x75,   0x01,

  0x81,   0x02,
  0x95,   0x01,
  0x75,   0x05,
  0x81,   0x01,

  0x05,   0x01,
  0x09,   0x30,
  0x09,   0x31,
  0x09,   0x38,

  0x15,   0x81,
  0x25,   0x7F,
  0x75,   0x08,
  0x95,   0x03,

  0x81,   0x06,
  0xC0,   0x09,
  0x3c,   0x05,
  0xff,   0x09,

  0x01,   0x15,
  0x00,   0x25,
  0x01,   0x75,
  0x01,   0x95,

  0x02,   0xb1,
  0x22,   0x75,
  0x06,   0x95,
  0x01,   0xb1,

  0x01,   0xc0
};

/**
  * @}
  */

/** @defgroup USBD_Custom_Private_Functions
  * @{
  */

/**
  * @brief  USBD_Custom_Init
  *         Initialize the Custom interface
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_Custom_Init(USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
  /* Open EP IN */
  USBD_LL_OpenEP(pdev, Custom_fat_EPIN_ADDR, USBD_EP_TYPE_BULK, Custom_fat_EPIN_SIZE);
  pdev->ep_in[Custom_fat_EPIN_ADDR & 0xFU].is_used = 1U;

  USBD_LL_OpenEP(pdev, Custom_config_EPOUT_ADDR, USBD_EP_TYPE_BULK, Custom_config_EPOUT_SIZE);
  pdev->ep_in[Custom_config_EPOUT_ADDR & 0xFU].is_used = 1U;

  USBD_LL_OpenEP(pdev, Custom_debug_EPIN_ADDR, USBD_EP_TYPE_BULK, Custom_debug_EPIN_SIZE);
  pdev->ep_in[Custom_debug_EPIN_ADDR & 0xFU].is_used = 1U;

  pdev->pClassData = USBD_malloc(sizeof(USBD_Custom_HandleTypeDef));

  if (pdev->pClassData == NULL)
  {
    return USBD_FAIL;
  }

  ((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state = Custom_IDLE;

  return USBD_OK;
}

/**
  * @brief  USBD_Custom_Init
  *         DeInitialize the Custom layer
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_Custom_DeInit(USBD_HandleTypeDef *pdev,
                                uint8_t cfgidx)
{
  /* Close Custom EPs */
  USBD_LL_CloseEP(pdev, Custom_fat_EPIN_ADDR);
  pdev->ep_in[Custom_fat_EPIN_ADDR & 0xFU].is_used = 0U;

  USBD_LL_CloseEP(pdev, Custom_config_EPOUT_ADDR);
  pdev->ep_in[Custom_config_EPOUT_ADDR & 0xFU].is_used = 0U;

  USBD_LL_CloseEP(pdev, Custom_debug_EPIN_ADDR);
  pdev->ep_in[Custom_debug_EPIN_ADDR & 0xFU].is_used = 0U;

  /* FRee allocated memory */
  if (pdev->pClassData != NULL)
  {
    USBD_free(pdev->pClassData);
    pdev->pClassData = NULL;
  }

  return USBD_OK;
}

/**
  * @brief  USBD_Custom_Setup
  *         Handle the Custom specific requests
  * @param  pdev: instance
  * @param  req: usb requests
  * @retval status
  */
static uint8_t  USBD_Custom_Setup(USBD_HandleTypeDef *pdev,
                               USBD_SetupReqTypedef *req)
{
  USBD_Custom_HandleTypeDef *hCustom = (USBD_Custom_HandleTypeDef *) pdev->pClassData;
  uint16_t len = 0U;
  uint8_t *pbuf = NULL;
  uint16_t status_info = 0U;
  USBD_StatusTypeDef ret = USBD_OK;

  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {
    case USB_REQ_TYPE_CLASS :
      switch (req->bRequest)
      {
        case Custom_REQ_SET_PROTOCOL:
          hCustom->Protocol = (uint8_t)(req->wValue);
          break;

        case Custom_REQ_GET_PROTOCOL:
          USBD_CtlSendData(pdev, (uint8_t *)(void *)&hCustom->Protocol, 1U);
          break;

        case Custom_REQ_SET_IDLE:
          hCustom->IdleState = (uint8_t)(req->wValue >> 8);
          break;

        case Custom_REQ_GET_IDLE:
          USBD_CtlSendData(pdev, (uint8_t *)(void *)&hCustom->IdleState, 1U);
          break;

        default:
          USBD_CtlError(pdev, req);
          ret = USBD_FAIL;
          break;
      }
      break;
    case USB_REQ_TYPE_STANDARD:
      switch (req->bRequest)
      {
        case USB_REQ_GET_STATUS:
          if (pdev->dev_state == USBD_STATE_CONFIGURED)
          {
            USBD_CtlSendData(pdev, (uint8_t *)(void *)&status_info, 2U);
          }
          else
          {
            USBD_CtlError(pdev, req);
            ret = USBD_FAIL;
          }
          break;

        case USB_REQ_GET_DESCRIPTOR:
          if (req->wValue >> 8 == Custom_REPORT_DESC)
          {
            len = MIN(Custom_MOUSE_REPORT_DESC_SIZE, req->wLength);
            pbuf = Custom_MOUSE_ReportDesc;
          }
          else if (req->wValue >> 8 == Custom_DESCRIPTOR_TYPE)
          {
            pbuf = USBD_Custom_Desc;
            len = MIN(USB_Custom_DESC_SIZ, req->wLength);
          }
          else
          {
            USBD_CtlError(pdev, req);
            ret = USBD_FAIL;
            break;
          }
          USBD_CtlSendData(pdev, pbuf, len);
          break;

        case USB_REQ_GET_INTERFACE :
          if (pdev->dev_state == USBD_STATE_CONFIGURED)
          {
            USBD_CtlSendData(pdev, (uint8_t *)(void *)&hCustom->AltSetting, 1U);
          }
          else
          {
            USBD_CtlError(pdev, req);
            ret = USBD_FAIL;
          }
          break;

        case USB_REQ_SET_INTERFACE :
          if (pdev->dev_state == USBD_STATE_CONFIGURED)
          {
            hCustom->AltSetting = (uint8_t)(req->wValue);
          }
          else
          {
            USBD_CtlError(pdev, req);
            ret = USBD_FAIL;
          }
          break;

        default:
          USBD_CtlError(pdev, req);
          ret = USBD_FAIL;
          break;
      }
      break;

    default:
      USBD_CtlError(pdev, req);
      ret = USBD_FAIL;
      break;
  }

  return ret;
}

/**
  * @brief  USBD_Custom_SendReport
  *         Send Custom Report
  * @param  pdev: device instance
  * @param  buff: pointer to report
  * @retval status
  */
uint8_t USBD_Custom_SendReport(USBD_HandleTypeDef  *pdev,
                            uint8_t *report,
                            uint16_t len)
{
  USBD_Custom_HandleTypeDef     *hCustom = (USBD_Custom_HandleTypeDef *)pdev->pClassData;

  if (pdev->dev_state == USBD_STATE_CONFIGURED)
  {
    if (hCustom->state == Custom_IDLE)
    {
      hCustom->state = Custom_BUSY;
      USBD_LL_Transmit(pdev,
                       Custom_debug_EPIN_ADDR,
                       report,
                       len);
    }
  }
  return USBD_OK;
}

/**
  * @brief  USBD_Custom_GetPollingInterval
  *         return polling interval from endpoint descriptor
  * @param  pdev: device instance
  * @retval polling interval
  */
uint32_t USBD_Custom_GetPollingInterval(USBD_HandleTypeDef *pdev)
{
  uint32_t polling_interval = 0U;

  /* HIGH-speed endpoints */
  if (pdev->dev_speed == USBD_SPEED_HIGH)
  {
    /* Sets the data transfer polling interval for high speed transfers.
     Values between 1..16 are allowed. Values correspond to interval
     of 2 ^ (bInterval-1). This option (8 ms, corresponds to Custom_HS_BINTERVAL */
    polling_interval = (((1U << (Custom_HS_BINTERVAL - 1U))) / 8U);
  }
  else   /* LOW and FULL-speed endpoints */
  {
    /* Sets the data transfer polling interval for low and full
    speed transfers */
    polling_interval =  Custom_FS_BINTERVAL;
  }

  return ((uint32_t)(polling_interval));
}

/**
  * @brief  USBD_Custom_GetCfgFSDesc
  *         return FS configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_Custom_GetFSCfgDesc(uint16_t *length)
{
  *length = sizeof(USBD_Custom_CfgFSDesc);
  return USBD_Custom_CfgFSDesc;
}

/**
  * @brief  USBD_Custom_GetCfgHSDesc
  *         return HS configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_Custom_GetHSCfgDesc(uint16_t *length)
{
  // *length = sizeof(USBD_Custom_CfgHSDesc);
  // return USBD_Custom_CfgHSDesc;
  *length = sizeof(USBD_Custom_CfgFSDesc);
  return USBD_Custom_CfgFSDesc;
}

/**
  * @brief  USBD_Custom_GetOtherSpeedCfgDesc
  *         return other speed configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_Custom_GetOtherSpeedCfgDesc(uint16_t *length)
{
  // *length = sizeof(USBD_Custom_OtherSpeedCfgDesc);
  // return USBD_Custom_OtherSpeedCfgDesc;
  *length = sizeof(USBD_Custom_CfgFSDesc);
  return USBD_Custom_CfgFSDesc;
}

/**
  * @brief  USBD_Custom_DataIn
  *         handle data IN Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_Custom_DataIn(USBD_HandleTypeDef *pdev,
                                uint8_t epnum)
{

  /* Ensure that the FIFO is empty before a new transfer, this condition could
  be caused by  a new transfer before the end of the previous transfer */
  ((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state = Custom_IDLE;
  return USBD_OK;
}

/**
  * @brief  USBD_Custom_DataOut
  *         Data received on non-control Out endpoint
  * @param  pdev: device instance
  * @param  epnum: endpoint number
  * @retval status
  */
static uint8_t  USBD_Custom_DataOut(USBD_HandleTypeDef *pdev, uint8_t epnum)
{
  USBD_Custom_HandleTypeDef   *hcustom = (USBD_Custom_HandleTypeDef *) pdev->pClassData;

  /* Get the received data length */
  hcustom->RxLength = USBD_LL_GetRxDataSize(pdev, epnum);

  /* USB data will be immediately processed, this allow next USB traffic being
  NAKed till the end of the application Xfer */
  if (pdev->pClassData != NULL)
  {
    // ((USBD_Custom_ItfTypeDef *)pdev->pUserData)->Receive(hcustom->RxBuffer, &hcustom->RxLength);

    return USBD_OK;
  }
  else
  {
    return USBD_FAIL;
  }
}

/**
* @brief  DeviceQualifierDescriptor
*         return Device Qualifier descriptor
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t  *USBD_Custom_GetDeviceQualifierDesc(uint16_t *length)
{
  *length = sizeof(USBD_Custom_DeviceQualifierDesc);
  return USBD_Custom_DeviceQualifierDesc;
}

/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
