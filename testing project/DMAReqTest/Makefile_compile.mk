# Copyright (C) 2019 Patrik Bachan @ eForce

all: $(OBJECTS)

%.o : %.c
$(BUILD_DIR)/%.o : $(SOURCE_ROOT)/%.c $(DEP_DIR)/%.d
	$(call action_ex, Building C  , $<\n)
	$(COMPILE.c) $<
	$(POSTCOMPILE)

%.o : %.s
$(BUILD_DIR)/%.o : $(SOURCE_ROOT)/%.s $(DEP_DIR)/%.d
	$(call action_ex, Building S  , $<\n)
	$(COMPILE.s) $<
	$(POSTCOMPILE)

%.o : %.cpp
$(BUILD_DIR)/%.o : $(SOURCE_ROOT)/%.cpp $(DEP_DIR)/%.d
	$(call action_ex, Building C++, $<\n)
	$(COMPILE.cpp) $<
	$(POSTCOMPILE)

$(DEP_DIR)/%.d: ;
.PRECIOUS: $(DEP_DIR)/%.d

-include $(DEPS)
