testing_notes.txt

TEST REPORT 2020-01-29
Objective
	Measure latency of a timer requesting DMA transfer from GPIO to SRAM on an STM32F7 to verify DMAMUX peripheral (STM32G0, STM32G3 series only) is not needed for the project
Device under test
	Nucleo with a STM32F756ZG
Gear
	Rohde & Schwarz HAMEG HMF2550 50MHz arbitrary generator
	Agilent DSO3102A 100MHz digital oscilloscope for input pulse verification
Setup
	RCC
		max clock 216MHz
	DMA
		DMA2 s6 ch0, circular to prevent interrupt clear in HAL functions (will do it without HAL functions next time, hard to modify). Transfer finished interrupt enabled with a handler, breakpoint in said handler to verify if GPIO pin has been read correctly. Source set to GPIOF input data register. Destination set to a variable named "destination", which had to be defined with __attribute__((__used__)) which is a gcc-specific trick to stop it from being optimized away. 
	TIM1
		TIM1 CH1 connected to signal generator output. Set up as TI1FP1 no filter rising edge, linked to trigger TIM1 CH3 in one pulse mode (OPM) PWM mode 2 on ch3. TIM1 running at 216MHz, no prescaler, slave mode is trigger mode, generating OC3REF DMA request. CH3 capturing at CCR3 54 [cycles] = 250ns, reloading at ARR 432 [cycles] = 2us. This generates on CH1 input being high a pulse on OC3REF delayed by 250ns, 1.75us long.
	GPIO
		GPIOF, a single input pin also connected to the signal generator output
	Signal generator
		Hi Z output, single pulse with variable high width, low level 0V, high level 3.3V, 100ms period to prevent DMA retrigger before the debugger reads out destination value on breakpoint in DMA IRQ handler
TIM capture compare with 
checking DMA destination on breakpoint in DMA interrupt handler
CONCLUSION
	Latency as defined in Objective was found to be 35-40ns, assuming symmetric edge times of signal generator (verified approximately visually on oscilloscope)
