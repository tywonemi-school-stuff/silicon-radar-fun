Version 1.3.11 (2019-11-27):
- bugfix: raw data output

Version 1.3.10 (2019-10-21):
- bugfix: target list output
- bugfix: negative bandwidth feature
- bugfix: length of downsampled data output
- bugfix: !M command input
- improved CFAR calculation
- improved DC cancellation

Version 1.3.9 (2019-05-27):
- bugfix: scaling for Mag/Phi values in !M frames, less zero content
- improved sampling and ADC/PLL synchronization
- improved ramptime calculation for more accurate ramptime
- removed switching disturbance at the end of the ramp group
- reduced spectrum jumping near DC

Version 1.3.8 (2019-03-15):
- added PLL trigger output on CN10/PC4 of the Nucleo/Easy

Version 1.3.7 (2018-02-15):
- bugfix: UART issue, sometimes commands would cause crash

Version 1.3.6 (2018-09-20):
- bugfix: commands sent via UART/WebGUI were sometimes not accepted anymore
- bugfix: first ramp deformed when certain settings are made, led to malformed spectrum output
- bugfix: crash in Extended Mode when certain settings are made
- reduced the delay time between ramps

Version 1.3.5 (2018-07-06):
- bugfix: frequency scan, min and max frequency were sometimes not correctly detected
- combined the 24 GHz and 120 GHz firmwares for the Easy kit into one
- added version info frame !V

Version 1.3.1 (2018-01-17):
- bugfix for Simple kit

Version 1.3 (2018-01-16):
- improved handling of short commands

Version 1.1 (2017-01-05):
- minor update for newer Easy hardware with TRX_120

Version 1.0 (2016-12-20):
- initial Firmware version
