 ================================
| License and source information |
 ================================
 1. Firmware and Drivers by Silicon Radar
 2. CMSIS Library by ARM
 3. STM32CubeMX F3 MCU Package by ST Microelectronics
 4. BuildNumber by John M. Stonham
 
 ==========================================
| 1. Firmware and Drivers by Silicon Radar |
 ==========================================

This firmware is provided by Silicon Radar for SiRad Easy� and SiRad Simple�. By using this software, you agree to our license conditions. Please find the license in this folder in the file LICENSE-SiRad.txt. 

We provide the source code of the baseband drivers for SiRad Easy� and SiRad Simple�, available (after registration) from

https://login.siliconradar.com/

The drivers code includes controls for the radar front end, PLL, data aquisition, a limited UART communication protocol, as well as a small application example for starting your custom application development.

 =========================
| 2. CMSIS Library by ARM |
 =========================
 
This firmware uses the CMSIS library for the Cortex-M3 by ARM under the Apache License 2.0. By using this software, you agree to the Apache License 2.0 conditions. Please find the license in this folder or under

https://github.com/ARM-software/CMSIS_5/blob/develop/LICENSE.txt

The source code of the CMSIS library is available from

https://github.com/ARM-software/CMSIS_5

 ===============================
| 3. STM32CubeMX F3 MCU Package |
 ===============================

This firmware uses the STM32CubeMX F3 MCU Package by ST Microelectronics. By using this software, you agree to ST's license conditions. Please find the license in this folder on the ST website under Legal -> LICENSE AGREEMENT:

https://www.st.com/en/development-tools/stm32cubemx.html#resource

The source code of the STM32CubeMX F3 MCU Package can be retrieved by installing STM32CubeMX, available from

https://www.st.com/en/development-tools/stm32cubemx.html#overview

 ====================================
| 4. BuildNumber by John M. Stoneham |
 ====================================

This firmware uses the BuildNumber library by John M. Stoneham under BSD License. By using this software, you agree to the BSD License conditions. Please find the license in this folder or under

https://github.com/icekobrin/fourdeltaone/blob/master/dependencies/src/buildnumber/LICENSE.TXT

The source code of BuildNumber is available from

https://github.com/icekobrin/fourdeltaone/tree/master/dependencies/src/buildnumber
