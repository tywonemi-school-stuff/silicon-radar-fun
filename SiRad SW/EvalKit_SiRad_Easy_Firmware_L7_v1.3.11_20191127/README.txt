 =======================
| Flashing the firmware |
 =======================
 1. SiRad Easy�
 2. SiRad Simple�

Please also consider our wiki pages at http://wiki.siliconradar.com and our download area at https://login.siliconradar.com.
 
 ================
| 1. SiRad Easy� |
 ================

Open the Windows Explorer. Use any standard USB cable to connect the kit to a free USB port of your PC. A new device should show up in the Windows Explorer, named 'NODE_F303RE'. Drag and drop the firmware .bin file on that device. LD1 on the board starts flashing red and green and a new window should display the flash progress. As soon as the window closes - after about 20s - the kit is flashed and the new firmware should be running, indicated by the blinking user LED on the green board.

If you need more help, also visit

https://siliconradar.com/wiki/First_steps

 ==================
| 2. SiRad Simple� |
 ==================

Please choose either step 2.1 - using a terminal program - OR step 2.2 - using the hardware switch - to set the kit into the programming mode, before flashing the firmware (step 2.3). If you experience problems with step 2.1, try step 2.2 instead.

 ==============================
| 2.1 Using a terminal program |
 ==============================

Open the Windows Device Manager and unfold the section 'Ports (COM & LPT)'. Use the FTDI cable included in the package to connect to kit to a free USB port of your PC. A new device should show up, named 'USB Serial Port (COMx)'. Remember the COM port number for the next steps. Get and install a terminal program, for example, Realterm from

https://sourceforge.net/projects/realterm/

Open the terminal program and connect to the kit with the following serial settings: 230400 baud, no parity, 8 data bits, 1 stop bit, no flow control, your COM port number. You should see data from the kit flowing. Send the following string as ASCII to the kit in order to set it in programming mode: !W\r\n

The kit should stop sending data and the LED should go off. IMPORTANT: disconnect and close your terminal program from the kit or the next steps might fail. Continue to step 2.3.

 ===============================
| 2.2 Using the hardware switch |
 ===============================

Disconnect the kit from the PC or the FTDI cable from the kit to power it off. Switch the MP switch of SW1 on the kit to on. Connect the kit again to the PC. The kit should not send any data now and the LED should stay off. IMPORTANT: Verify that no other program is connected to the COM port of the kit or the next steps might fail. Continue to step 2.3.

 ========================
| 2.3 Flash the firmware |
 ========================

Go to the ST website, download and install STM32CubeProgrammer:
 
https://www.st.com/en/development-tools/stm32cubeprog.html

Open STM32CubeProgrammer and connect to the kit with the following UART options: your COM port number, 115200 baud, even parity. Drag and drop the firmware .bin file into STM32CubeProgrammer. Click the 'Download' button and wait for the flash process to finish. Disconnect STM32CubeProgrammer from the kit. Continue to step 2.4.

 =====================
| 2.4 Restart the kit |
 =====================

If you used step 2.1, you are finished. 

If you used step 2.2, disconnect the kit from the PC again. Switch the MP switch of SW1 on the kit to off. Connect the kit again to the PC. Your are finished flashing the firmware.
