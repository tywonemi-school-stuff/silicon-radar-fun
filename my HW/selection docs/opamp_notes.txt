opamp_notes.txt

input decoupled with 100nF gives 0.5Hz Re{Z}=1/2pifC=1/(pi*2*0.5*100*10^-9) = 1/(pi*10-7) = pi MOhm
let's allow 500uA load from LM4040 into a -Vref. This gives 8kOhm min top resistor, bottom is to bias to half Vref so output is at one Vref. For safety let's go for 22k, since ADC input is clamped anyway

LMH6628 crosstalk is -62dB, acceptable for the application