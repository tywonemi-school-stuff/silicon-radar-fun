\subsection{ADC characteristics}
From specification of the original Nucleo F303 solution and specifications of the external ADC on the custom design, typical values are provided in Table \ref{tab:adccompartable}. With both input channels left floating, low noise of the sampling system has been observed, as can be seen in the raw dump of the 2D spectrum plot in Figure \ref{fig:adcquiet}. Compare with the bright, easily discernible signals in Figure \ref{fig:viewerscreenshot}. A shortcoming of hardware version 1.1 is its strong attenuation of low frequency signals - even going from 1 to 2MHz produces a visible increase in signal strength. This can easily be fixed with the input AC-coupling capacitor value being reduced.
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{quiet.png}
    \caption{2D spectrum plot of a batch of samples, input channels left floating, image unscaled}
    \label{fig:adcquiet}
\end{figure}
\begin{table}[]
    \begin{ctucolortab}
    \begin{tabular}{lcc}
    \bfseries System & \bfseries Nucleo F303 & \bfseries SiRad ADC v1 \\
    \bfseries Nominal bit-depth & 12-bit & 12-bit \\
    \bfseries Offset error & $\pm$1 LSB & -0.1\% FS \\
    \bfseries Gain error & $\pm$3 LSB & $\pm$0.2\% FS \\
    \bfseries Differential linearity error & $\pm$1 LSB & $\pm$0.3 LSB \\
    \bfseries Integral linearity error & $\pm$1.5 LSB & $\pm$0.5 LSB \\
    \bfseries Effective number of bits & 10.8 bits & 11.4 bits \\
    \bfseries Signal-to-noise and distortion ratio & \SI{68}{\deci\bel} &  \SI{70}{\deci\bel} \\
    \bfseries Signal-to-noise ratio & \SI{67}{\deci\bel} & \SI{70}{\deci\bel} \\
    \bfseries Total Harmonic Distortion & -\SI{76}{\deci\bel} & -\SI{86}{\deci\bel} \\ %% what the HELL is siunitx doing? Why does it hate negative numbers? What the hell?!
    \bfseries Intermodulation Distortion & unspecified & -\SI{75}{\deci\bel}FS \\
    \end{tabular}
    \end{ctucolortab}
    \caption{Comparison of specified sampling performance, typ. values}
    \label{tab:adccompartable}
\end{table}
% Electronic 
\subsection{Comparison with existing solutions}
This section introduces a few off-the-shelf data acquisition systems, to provide context for the capabilities of the SiRad ADC v1 board.\par 
The ubiquitous \textbf{Saleae logic analyzer clones}\cite{saleae}\cite{eevsaleae}\cite{sigroksaleae}, popular for debugging digital buses, provides 8 channels, sampled at up to \SI{24}{\mega\hertz} over a 192Mbps USB-HS connection. It holds no firmware, as it is loaded with a program from the host PC. Its microcontroller is a low-cost legacy 8051 part with scarce peripherals. It has no detection of dropped frames, that could be caused by the host PC's USB controllers introducing latency, and its data buffer is, at most, \SI{4}{\kibi\byte}$+$\SI{8.5}{\kibi\byte}. Its core runs at \SI{48}{\mega\hertz} maximum and as such can not be used for reliable logic analysis, or radar baseband sampling, unless dropped frame detection is built into PC software.\par 
Beginner radio frequency enthusiasts' choice, the \textbf{RTL2832U}, is an integrated solution for DVB-T broadcast television reception. It is bundled with various external tuner ICs, which it controls. It samples the analog downconverted intermediate frequency signal, further downconverts it digitally into an I/Q signal pair, which it downsamples and serves over USB High Speed. Its theoretical maximum sampling rate is 3.2 Msps, but typically only 2.4 Msps can be achieved due to packet loss. With \SI{8}{\bit} per I and Q channel each, the theoretical maximum USB throughput of the device is 51.2Mbps. Packet loss can be checked with rtl\_test, a utility in the librtlsdr toolkit\cite{librtlsdr}, and implemented in host code. However, no dropped frame detection has been found in the rtl\_sdr backend used by utility scripts and applications, or GUI applications using various backends such as SDR\# -- in operation, it is assumed one has tested the connection and sampling rate used, and that these conditions remain the same.\par 
Modern digital oscilloscopes operate at bandwidths ranging from \SI{50}{\mega\hertz} to \SI{100}{\giga\hertz}\cite{lecroy}. However, they are not designed to stream captured data in real time at their specified sampling rates to an external machine. Higher-end models certainly are built on full-featured desktop computers with ample processing power, but are sold as monolithic devices that are highly impractical for use outside of the laboratory. However, their fast data acquisition core components can be found as PCIe cards or modules for PCIe-like systems in the price range of 1300-30000EUR. A subversive exception to this is \textbf{ScopeFun}\cite{scopefun}, an open source oscilloscope with two 10-bit channels sampled at 250 Msps each. If only one channel is used, it delivers 500 Msps over its USB 3.0 SuperSpeed, saturating the bus 5Gbps bandwidth. Its functionality relies on a Xilinx Artix-7 FPGA with 512MB RAM, and an external Cypress FX3 USB 3.0 controller.
\begin{table}[h]
    \begin{ctucolortab}
    \begin{tabular}{llllll}
    \bfseries System & \bfseries Sampling rate & \bfseries Throughput & \bfseries ADC & \bfseries Cost \\
    Saleae clone & 24 MHz      & 192 Mbps      & no     & 10 EUR \\
    RTL-SDR      & 3.2 MHz     & 52.2 Mbps     & 8-bit  & 16 EUR \\
    SiRad ADC v1 & 5 MHz       & 160 Mbps      & 12-bit & 70 EUR$^\ast $\\
    ScopeFun     & 500 MHz     & 5 Gbps        & 10-bit & 660 EUR / 300 EUR$^\ast $
    \end{tabular}
    \end{ctucolortab}
    \caption{Comparison of acquisition systems. $^\ast$ cost of manufacture in quantity of 5}
    \label{tab:compartable}
\end{table}

\subsection{Required processing for on-board 2D FFT}
Detecting targets from M chirps (of N samples each) on an embedded system requires:
\begin{itemize}
    \item pre-processing
    \item M x FFT of size N
    \item transposition in RAM
    \item N x FFT of size M
    \item post-processing
\end{itemize}
The Cortex-M7 core in the STM32F7 series implements the Armv7-M instruction set as a part of the Armv7E-M microarchitecture, which features a two-way superscalar architecture with integer SIMD (single instruction, multiple data) operations on integers among its digital signal processing capabilities. It also has access to a floating-point unit (FPU) implementing the FPv5 instruction set extension.\cite{cortexm7progman} We can take a look at how this works in practice in a report by ST Microelectronics for Cortex-M7 with a comparable device, the STM32F746\cite{stm32fft}. Its processing power differs from the STM32F733 part used in this device only in that it has half the instruction and data cache. This benchmark uses ARM CMSIS-DSP library functions, which are considered the canonical implementations of mathematical functionality, making use of SIMD capabilities, presumably optimally. The results show that a Cortex-M7 is able to compute a 1024-point FFT in \SI{339}{\micro\second} in 32-bit floating-point representation, and in \SI{265}{\micro\second} in the Q15 fractional format which does not require the FPU, see Figure \ref{fig:stm32f7fft}. On the slightly lower clock speed, non-superscalar Cortex M4 part, a 50\% increase in cycle count per operation has been observed \ref{fig:stm32f4fft}.  Floating-point is preferable due to its high possible dynamic range and lower error propagation risk. \par
From the requirements for this device, $N=1024$ and $M=128$. The on-board device will take at least \SI{43}{\milli\second} to compute our Mx N-point FFT. If we extrapolate from the available data the processor time for a 128-point FFT by determining the square root of the ratio between 256 and 64-point, we get an estimate of \SI{35}{\micro\second}, adding up to \SI{35}{\milli\second} for the Nx M-point FFT operation.\par
Matrix transposition is a costly operation. Note that we can't store and operate on our data in TCM due to DMA access limitations. The \SI{8}{\kilo\byte} data cache is not useful for SRAM accesses located on non-incrementing addresses; a matrix transposition is going to require copying into/from an incrementing 1024 bytes-long array from/into addresses offset by 1024 bytes. This means that just about every operation will be at SRAM speeds, which does not have specified access times. In reality, the situation is even somewhat more complicated due to complex FFT requiring interleaved real and imaginary parts of the values.\par
We can already see that just the raw FFTs are going to take \SI{78}{\milli\second}. With the transposition and additional overhead, it would be challenging to achieve just \SI{10}{\hertz}, which is poor for control-theoretical applications as well as human operation. Additional overhead would come from the required memory operations left out of these calculations, as they can not be easily specified without being implemented and benchmarked. \par
Thankfully, there is a wide range of embedded computing systems available. If we limit ourselves to the realm of ARM, the next performance tier are the lower-end Cortex-A cores. The Nvidia Jetson Nano development kit was used to benchmark FFT performance of its modern quad-core ARMv8-A 64-bit out-of-order superscalar Cortex-A57 processor running at \SI{1.5}{\giga\hertz}, featuring a VFPv4 vector FPU for each core, and plentiful two-layer caching. Using the NE10 library, optimized for ARM vector compute\cite{ne10}, a 1024-point FFT to take \SI{17}{\micro\second} in a single-threaded task, at \SI{1.5}{\giga\hertz} with a power consumption under \SI{3}{\watt}. The benchmark program is modified F32 complex FFT sample code from the NE10 library and is provided in the attached project files.\par
In comparison with a modern PC system: 1024-point complex FFT took \SI{2.6}{\micro\second} on a \SI{4.3}{\giga\hertz} on a single core of the Intel i7-9750H (microcode update 0xCA) laptop CPU, driving the processor's power consumption \SI{23}{\watt} above its idle power consumption of \SI{4}{\watt} using the FFTW library under Windows 10.0.18363 wrapped in pyFFTW, run with python 3.7.4. Furthermore, compiled native C code using single-core FFTW executing 1024-point complex DFTs has been measured against parallelized CUDA with the CuFFT library. \par
CUDA\cite{cuda} is a high core-core count architecture, primarily designed for graphical processing on vectored floats, but extensively used for short integer operations for machine learning, and other GPGPU (general purpose GPU) usecases. Taking a task and offloading it to the external resource, the GPU, typically brings latency overhead, and the total execution time is dependent not on the average unit execution time, but the worst unit execution time, so parallelizing operations comes with its own set of dangers. The overhead and lower per-core performance of CUDA caused CuFFT to be approximately half as fast as FFTW at 1024-point, and only gain an upper hand at 4096-point and larger. However, CUDA allows us to perform many operations in parallel. \par 
On the personal computer system, CuFFT on the 896-core GTX 1650 performed 128 1024-point FFTs in a time equivalent to \SI{172}{\nano\second} of sequential execution, outperforming the straight-C FFTW time of \SI{2.14}{\micro\second}. On the Jetson Nano, CuFFT on the 128-core GPU performed 128 1024-point FFTs in a sequential equivalent time of \SI{1.5}{\micro\second}, outperforming the single-threaded NE10 at \SI{17}{\micro\second}. This demonstrates that many-core solutions are a potentially highly power-efficient fit for radar systems. All used code is provided for replication in the attachments in FFT and fftw-vs-cufft subdirectories of the testing directory. FFTW vs CuFFT is a modified version of a free benchmarking setup by Github user moznion.\cite{cufftbench} \par
If we wish to identify targets, peak detection has to be implemented. A popular option is CFAR (constant false alarm rate algorithm), a stateless detector, the simplest instance of which triggers when a value in a "cell" is higher than the cell value average with an added threshold. When peaks tend to be wider than one cell, a "guard cell" pattern is introduced, which prevents points close to a peak from registering as peaks.\par
Let us evaluate the computational complexity of CFAR without guard cells. With the resulting range-velocity plot still at a size of $M \times N$, let our cells have the size of $P \times P$ points. To get cell averages, we do $M \times N$ mostly fusable sums and $P^2$ divisions. At a Cortex-M7 core clock of \SI{216}{\mega\hertz}, assuming 1-3 clocks per each of these instructions, this creates a latency of under \SI{5}{\milli\second}, negligible in comparison with the FFTs.\par

\begin{figure}[]
    \centering
    \includegraphics[width=\columnwidth]{stm32f7fft.png}
    \caption{FFT computation time on a Cortex-M7 device at 216MHz with instruction and data caches enabled\cite{stm32fft}}
    \label{fig:stm32f7fft}
\end{figure}
\begin{figure}[]
    \centering
    \includegraphics[width=\columnwidth]{stm32f4fft.png}
    \caption{FFT computation time on a Cortex-M4 device at 180MHz\cite{stm32fft}}
    \label{fig:stm32f4fft}
\end{figure}
It should be noted, that ARM Helium, a low power DSP instruction set extension, will be available for cores implementing the upcoming Armv8.1-M microarchitecture.\cite{armv8m_archref} Out of these cores, only one, the Cortex-M55, has been announced. The floating-point version (MVE-F) of Helium will bring, among other things, support for half-precision float vector operations, and most likely noticable speedup of DSP loops due to removed overhead. This might somewhat change the playing field of high complexity, very low power computation in embedded systems.\par
\pagebreak
The original evaluation kit firmware gets the frequency spectrum of the incoming data from each chirp, averages the chirp spectra, and detects peaks with a CFAR algorithm with configurable parameters. This is in practice grossly insufficient for velocity estimation.
\subsection{DMA request latency}
\par
The latency of the  DMA transfer itself can be estimated using manufacturer-povided documentation\cite{stmandma} to be 5 AHB clocks in the case of an AHB peripheral such as GPIO, which comes out to be \SI{23}{\nano\second} with the maximum core and AHB clock of \SI{216}{\mega\hertz}. This for copying the IDR of a single GPIO bank. We are copying two, sequentially, and there is a risk of this trigger coinciding with an interrupt, adding further cycles, up to an estimate of 13 cycles until both registers are copied into SRAM, corresponding to \SI{60}{\nano\second}. With a \SI{5}{\mega\hertz} sampling rate, our latched ADC codes are constant on the GPIO inputs for \SI{100}{\nano\second}. Our transfers fit into this timeframe.\par
However, this documentation does not cover the latency of the transfer being triggered. For this reason, before the device was built, a simple feasibility test was conducted to determine how fast and reliably can a DMA transaction be triggered by a timer event on an empty bus, since it was not clear if there was a need for the DMAMUX peripheral, which only available in newer STM32 parts. It was only needed to establish an upper bound, so some liberties in inaccuracy have been taken. A Nucleo board with a STM32F756ZG microcontroller was used, with the following configuration:
\pagebreak
\begin{itemize}
    \item Rohde \& Schwarz HAMEG HMF2550 50MHz arbitrary generator, generating a variable-length 3.3V pulse every \SI{100}{\milli\second}, 
    \item GPIOF with a single input pin connected to the signal generator output
    \item GPIOE with a single input pin, which corresponds to the TIM1 channel 1, used as timer input 1
    \item TIM1 timer peripheral running at \SI{216}{\mega\hertz} (i.e. max bus clock) with 0 prescaler (no division)
    \item TIM1 channel 1 set up as TI1FP1 (timer input 1) no filter, rising edge, linked to:
    \item TIM1 channel 3 in one pulse mode (OPM) PWM mode 2, slave mode set to trigger, generating an OC3REF output compare event on capture at CNT = CCR3 = 54 cycles, corresponding to \SI{250}{\nano\second}, reloading arbitrarily at 432 cycles (\SI{2}{\micro\second}).
    \item DMA2 peripheral, stream 6 channel 0, circular mode (to prevent interrupt handler from resetting the transfer finished interrupt enable bit)
    \item DMA2 source: GPIO bank F input data register
    \item DMA2 destination: an allocated word in RAM with the GNU C Compiler "unused attribute" macro
    \item DMA2 trigger: the OC3REF event
    \item Breakpoint in said DMA's transfer complete interrupt handler
\end{itemize}
\vfill
The exact used source code is provided in the DMAReqTest subfolder of the repository and attachment.
The sequence of events in each run is as presented in Figures \ref{fig:dmareq_1}, \ref{fig:dmareq_0}:\par
% \pagebreak
\begin{figure}[]
    \centering
    \makebox[\textwidth]{\includegraphics[width=\columnwidth]{dmareqtest1.png}}
    \caption{Generated pulse long enough}
    \label{fig:dmareq_1}
\end{figure}
\begin{figure}[]
    \centering
    \makebox[\textwidth]{\includegraphics[width=\columnwidth]{dmareqtest0.png}}
    \caption{Generated pulse too short}
    \label{fig:dmareq_0}
\end{figure}
The generator output goes high. It is connected to the GPIOE pin, sets TI1, which triggers the timer 1. Timer 1 starts counting up, until its CNT register reaches CCR3. At that point, OC3REF is set. This triggers the DMA transfer, which copies the state of the GPIOF pin into RAM. This sequence takes $t_{seq} = t_1 + t_{timer} + t_2$, where we perfectly know $t_{timer}$ with a precision equal to that of the microcontroller crystal oscillator. $t_2$ includes the DMA request latency. From the timing diagram, we can see, that when the generator pulse time $t_{gen}$ is lesser than $t_{seq}$, we read a zero on the GPIOF pin, and a one if it is greater. By varying $t_{gen}$, we can deduce, that the bit flip happens repeatably (N=50) at $t_{gen} = t_{timer} \pm \SI{40}{\nano\second}$. The precision of the measurement was limited by the limited slew rate of the generator's output, and its pulse width being possible to change only in \SI{5}{\nano\second} increments. Symmetricity of the pulse edges was verified informally with an Agilent DSO3102A 100MHz digital oscilloscope, eliminating most of the distrust in the absolute value of the measurement. Regardless, this has proven that a DMA can read parallel data from GPIO buses reliably on an empty bus without, within the timeframe of \SI{200}{\micro\second} that a 5 Msps ADC requires, without the DMAMUX peripheral.
\subsection{I/Q equalization}
\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{trx_124_block.png}
    \caption{Block diagram of the 24GHz transceiver\cite{trx_24_dsh}}
    \label{fig:trx24block}
\end{figure}
The core part of the 24GHz RF frontend, the TRX\_024\_006 IQ Transceiver, generates a sine wave in the 24GHz band offset by a frequency by the Vctrl analog input, see Figure \ref{fig:trx24block}. It then generates an I/Q pair from this signal to downconvert the received signal with. However, it does not do so ideally, the part is specified to have a gain error of $\pm$\SI{1}{\deci\bel}, and a phase error of $\pm$\SI{10}{\degree}.\par
\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{iq_eq.png}
    \caption{I/Q imbalance creates ghost targets. a) uncalibrated, b) calibrated. From Ulrich, Yang 2017\cite{IQcal}}
    \label{fig:iq_eq}
\end{figure}
For example, easily implementable IQ gain and phase calibration introduced in Zekkari, Djendi, Guessoum 2019\cite{Zekkari2019} conclude that the following estimators noticably improve SNR in digital communications:
\begin{align}
    \hat{g}_{I}&=\sqrt{\frac{E\left\{y_{I}^{2}(n)\right\}}{E\left\{r_{Q}^{2}(n)\right\}}}=\sqrt{\frac{E\left\{y_{I}^{2}(n)\right\}}{E\left\{r_{I}^{2}(n)\right\}}}\\
    \hat{\phi}_{Q}&=\arcsin \frac{E\left\{y_{I}(n) y_{Q}(n)\right\}}{\sqrt{\frac{E\left\{y_{I}^{2}(n)\right\}}{E\left\{r_{I}^{2}(n)\right\}} \sqrt{\frac{E\left\{y_{Q}^{2}(n)\right\}}{E\left\{r_{I}^{2}(n)\right\}} E\left\{r_{I}^{2}(n)\right\}}}} \\
    &=\arcsin \frac{E\left\{y_{I}(n) y_{Q}(n)\right\}}{\sqrt{E\left\{y_{I}^{2}(n)\right\}} \sqrt{E\left\{y_{Q}^{2}(n)\right\}}}
\end{align}
where $y$ is local oscillator output and $r$ is received signal. We can eliminate the unknown received signal power, since we only care about the ratio $\frac{g_I}{g_Q}$. This method can continually calibrate for slowly changing error values, and should be considered in embedded real-time target detecting systems.