\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{hardware_arch.png}
    \caption{Hardware architecture}
    \label{fig:hwarch}
\end{figure}
\subsection{Inputs}
The device features two \SI[]{50}[]{\ohm} BNC inputs, coherently sampled, to be used as I/Q feeds or separate input channels. These are AC-coupled, DC-biased to the \SI{2.048}{\volt} ADC voltage reference. This allows for an input voltage range of 0--4.096V, that is, 2.048V$_{pp}$. In the next step, they are buffered, and filtered, before being sampled by the ADC. This is done to minimize the internal capacitance dynamical switching of the ADC from generating noise on the signal. The evaluation kit's baseband I/Q outputs are biased to \SI{1.65}{\volt}, and clipped to $\left<0;3.3\right>$V. Therefore this output range is fully covered by our input range.\par
The operation amplifiers, used as unity gain followers, were initially chosen to be LMH6628MAX. However, since these are grossly not rail-to-rail, and would require adding a negative voltage source to prevent clipping or distorting the signal, they have been replaced in v1.1 by a single rail-to-rail TLV3542ID. The latter is designed for lower voltage ranges, with specification equal or superior to the LMH6628MAX. Note that LMH6628MAX amplifiers were initially chosen due to being explicitly recommended by the ADC12010 datasheet, are in fact dual channel, but on v1 there are two on the board with their second channels ignored, due to the crosstalk of the channels being above the noise floor of the ADC. The TLV amplifiers have sufficiently low crosstalk in order for a single one to be used. \par
\subsection{USB}
Since a USB High Speed connection was required, to simplify the design, the STM32F733ZI microcontroller has been selected for its on-board USB High Speed physical layer. Other parts require an external interface integrated circuit connected over the "UTMI Low Pin Interface", the data and control lines of which are mapped out to seemingly random pins on STM32s, and require either a second on-board crystal, or careful use of the PLL output functionality of the STM32.
\subsection{Sampling}
\begin{figure}[H]
    \centering
    \includegraphics[width=270pt]{jesd204.png}
    \caption{Voltage vs. current-driven signaling in ADC outputs\cite{jesd204}}
    \label{fig:jesd204}
\end{figure}
Let us first consider the output bus, on which the digital data is to be carried to STM32F733ZI, the chosen microcontroller. If we look at available parts that meet our sampling rate and bit depth requirements, we find that the following options: classic voltage-driven parallel, serial such as I2C and SPI including its variants such as QSPI and Microwire, and high bandwidth parallel low voltage differential signaling (LVDS) layers such as JESD204. LVDS allows for lower power consumption at ridiculously high sampling rates, as demonstrated in Figure \ref{fig:jesd204}. There are no LVDS transceivers on the STM32 line of microcontrollers, an external LVDS physical would be connected in parallel, and the LVDS would be run for only ~\SI{2}{\centi\metre} on the board, defeating its purpose. On the STM32 F7 series, for this application, I2C is out of the question with its 1Mbps\cite{f733dsh} bandwidth limit, as well as SPI, with its limit of 54Mbps. At 54Mbps, getting a 12-bit ADC value would take \SI{222}{\nano\second}, which not just exceeds a reasonable timing window within the period of the 5 Msps sampling rate, it outright exceeds the period. For these reasons, a parallel bus has been selected. The STM32F7 GPIOs propagate input voltage to the internal registers at speeds exceeding \SI{100}{\mega\hertz}. \par

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{adc12010block.png}
    \caption{ADC12010 block diagram\cite{adcdsh}}
    \label{fig:adcblock}
\end{figure}
After the output was decided, a part that fulfills requirements was selected, the ADC12010, a 10 MSPS single channel, differential input, general purpose ADC.\cite{adcdsh} It features a pipelined architecture, converting input voltage to 22-bit with an internal sample and hold input stage, built-in unspecified digital correction to 12-bit, and a non-latching digital output buffer. The output is again buffered with a set of high-speed non-inverting octal buffers. Note that in the design, ACT-series drivers have been used in error, operating below their specified 4.5V-5.5V power supply range -- LVC series parts should be used instead, i.e. the 74LVC574ADB. Missed ADC codes may occur, since propagation delay is unspecified outside the operating voltage range, or rather, propagation isn't guaranteed at all. At 3.3V, a 3.5ns propagation delay is specified for 74LVC574Axx. Similarly, LVC-family Schmitt triggers are used to provide a high slew rate transition on clock inputs of the ADC and latches.
\subsection{Power consumption}
With its typical power draw of up to \SI{500}{\milli\ampere}, this device violates the USB standard, due to unimplemented link power management and excessive capacitive load on VBUS. ADCs are specified to draw \SI{160}{\milli\watt} each at 10 Msps and \SI{25}{\milli\watt} on power-down, which is unimplemented due to a possible bug in the microcontroller. Three different STM32F733ZI have been observed to be unable to set or read correctly configured GPIO pins on some of the banks, luckily not GPIOE and GPIOF, which are used as parallel buses for ADC data input. Power efficiency is also reduced by the use of a linear \SI{3.3}{\volt} regulator, selected for simplicity of design and lower noise over switch-mode supplies.
\subsection{Physical realization}
\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{boardv11.png}
    \caption{Hardware v1.1}
    \label{fig:board}
\end{figure}
As simple as the design is, it has been designed as a four-layer board with care taken to minimize signal crossing and a an internal all-GND layer as well as an internal all-power layer. USB-HS was length-matched to within \SI{2}{\milli\metre}, easily meeting the maximum skew of \SI{100}{\pico\second}\cite{usbspec2}. All layout guidelines have been followed as specified by the documentation for the ADC and microcontroller. Ferrite beads were used to minimize high slew rate current changes from the digital driving circuitry from causing propagation of voltage spikes. Digital connections to the system, with the exception of USB, have been protected with EMI5208MU eight-channel pi-filters, providing \SI{100}{\ohm} in-line DC resistance, EMI filters with a \SI{250}{\mega\hertz} cutoff frequency, and ESD protection. Note that due to a layout error in v1 hardware, a surge pulse isn't forced to travel through the surge protector. USB has been protected with the USBLC6-2 ESD and surge protection package, optimized for USB 2.0. Basic USB EMI/EMC reduction has been achieved with a standard common-mode choke. 
% \f@size