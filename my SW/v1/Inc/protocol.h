/**
 * @file protocol.h
 * @author Emil J Tywoniak (emil.tywoniak@evolvsys.cz)
 * @brief Custom USB protocol functionality
 * @date 2020-06-25
 * 
 * @copyright Emil J Tywoniak, licensed under LGPLv3
 * 
 */

#ifndef __PROTOCOL_H
#define __PROTOCOL_H

#include "stm32f7xx_hal.h"

#define BIG_BUFFER __attribute__ ((section (".sram_d1")))
// #define BUFFSIZE (12U*1024U) /* 12kWords = 48kB, buffer fills in 2.4 milliseconds with 5Msps */
#define BUFFSIZE (16U*1024U)

extern uint16_t buffer_I0[BUFFSIZE];
extern uint16_t buffer_I1[BUFFSIZE];
extern uint16_t buffer_Q0[BUFFSIZE];
extern uint16_t buffer_Q1[BUFFSIZE];

void Protocol_init(void);
void Protocol_waitForConfig(void);
void Protocol_waitForChirpStart(void);
void Protocol_chirp(void);
void Protocol_sendI0(struct __DMA_HandleTypeDef * hdma);
void Protocol_sendI1(struct __DMA_HandleTypeDef * hdma);
void Protocol_sendQ0(struct __DMA_HandleTypeDef * hdma);
void Protocol_sendQ1(struct __DMA_HandleTypeDef * hdma);
// void Protocol_send(void);
// void Protocol_fill(void);
#endif