/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include <stdbool.h> /* :boolin: */
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern TIM_HandleTypeDef htim1;
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define I2_Pin GPIO_PIN_2
#define I2_GPIO_Port GPIOE
#define I3_Pin GPIO_PIN_3
#define I3_GPIO_Port GPIOE
#define I4_Pin GPIO_PIN_4
#define I4_GPIO_Port GPIOE
#define I5_Pin GPIO_PIN_5
#define I5_GPIO_Port GPIOE
#define I6_Pin GPIO_PIN_6
#define I6_GPIO_Port GPIOE
#define Q0_Pin GPIO_PIN_0
#define Q0_GPIO_Port GPIOF
#define Q1_Pin GPIO_PIN_1
#define Q1_GPIO_Port GPIOF
#define Q2_Pin GPIO_PIN_2
#define Q2_GPIO_Port GPIOF
#define Q3_Pin GPIO_PIN_3
#define Q3_GPIO_Port GPIOF
#define Q4_Pin GPIO_PIN_4
#define Q4_GPIO_Port GPIOF
#define Q5_Pin GPIO_PIN_5
#define Q5_GPIO_Port GPIOF
#define Q6_Pin GPIO_PIN_6
#define Q6_GPIO_Port GPIOF
#define Q7_Pin GPIO_PIN_7
#define Q7_GPIO_Port GPIOF
#define Q8_Pin GPIO_PIN_8
#define Q8_GPIO_Port GPIOF
#define Q9_Pin GPIO_PIN_9
#define Q9_GPIO_Port GPIOF
#define Q10_Pin GPIO_PIN_10
#define Q10_GPIO_Port GPIOF
#define Q11_Pin GPIO_PIN_11
#define Q11_GPIO_Port GPIOF
#define I7_Pin GPIO_PIN_7
#define I7_GPIO_Port GPIOE
#define I8_Pin GPIO_PIN_8
#define I8_GPIO_Port GPIOE
#define I9_Pin GPIO_PIN_9
#define I9_GPIO_Port GPIOE
#define I10_Pin GPIO_PIN_10
#define I10_GPIO_Port GPIOE
#define I11_Pin GPIO_PIN_11
#define I11_GPIO_Port GPIOE
#define nOE_ADC_Pin GPIO_PIN_8
#define nOE_ADC_GPIO_Port GPIOC
#define PD_ADC_Pin GPIO_PIN_9
#define PD_ADC_GPIO_Port GPIOC
#define CLK_ADC_Pin GPIO_PIN_8
#define CLK_ADC_GPIO_Port GPIOA
#define I0_Pin GPIO_PIN_0
#define I0_GPIO_Port GPIOE
#define I1_Pin GPIO_PIN_1
#define I1_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#define trigger_PIN GPIO_PIN_15
#define trigger_GPIO_Port GPIOA

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
