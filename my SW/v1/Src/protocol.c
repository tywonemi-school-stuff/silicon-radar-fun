/**
 * @file protocol.c
 * @author Emil J Tywoniak (emil.tywoniak@evolvsys.cz)
 * @brief Custom USB protocol functionality
 * @date 2020-06-25
 * 
 * @copyright Emil J Tywoniak, licensed under LGPLv3
 * 
 */

#include "protocol.h"
#include "main.h"

#include "usbd_custom.h"

#define THRESH 5 /* After how many missed buffer misses we give up */
#define BYTES_PER_PACKET Custom_ADC_I_EPIN_SIZE
#define PACKETS_PER_FRAME 10 /* how many packets sent per frame*/

/* SRAM buffers */
/* Align the location of our buffers to a multiple of halfword size for correct DMA access */
uint16_t buffer_I0[BUFFSIZE] BIG_BUFFER __attribute__ ((aligned (2)));
uint16_t buffer_I1[BUFFSIZE] BIG_BUFFER __attribute__ ((aligned (2)));
uint16_t buffer_Q0[BUFFSIZE] BIG_BUFFER __attribute__ ((aligned (2)));
uint16_t buffer_Q1[BUFFSIZE] BIG_BUFFER __attribute__ ((aligned (2)));

/* Peripheral handles */
extern PCD_HandleTypeDef hpcd_USB_OTG_HS;
extern USBD_HandleTypeDef hUsbDeviceHS;
extern USBD_Custom_HandleTypeDef   *hcustom;
static USBD_HandleTypeDef* pdev;
uint32_t USBx_BASE;

/* Amount of data to write */
static uint32_t samples;
static uint32_t chirps;

/* Buffers to write */
static uint32_t bufferCount;

/* Buffers for low data endpoints */
char notifBuffer[Custom_notif_EPIN_SIZE] = "";
char configBuffer[Custom_config_EPOUT_SIZE] = "";

/* Transfer ending tools */
static bool Protocol_chirpIDone(void);
static bool Protocol_chirpQDone(void);
static bool doneI;
static bool doneQ;

/* How many buffer writes we sent and failed to send */
struct Stats {
  uint32_t i0;
  uint32_t i1;
  uint32_t q0;
  uint32_t q1;
  uint32_t i0miss;
  uint32_t i1miss;
  uint32_t q0miss;
  uint32_t q1miss;
} stats;

/**
 * @brief Initialize USB protocol module
 * 
 */
void Protocol_init() {
  USBx_BASE = (uint32_t)USB_OTG_HS;
  pdev=(USBD_HandleTypeDef*)hpcd_USB_OTG_HS.pData;
}

/**
 * @brief Send a chirp train to PC
 * 
 * @param samples samples per chirp
 * @param chirps chirps number in chirp train
 */
void Protocol_chirp() {
  /* Notify the PC */
  sprintf(notifBuffer, "chirp:0x%08lX;0x%08lX", samples, chirps);

  /* Notify about incoming chirp samples, if the interface is clean */
  if ( (USBx_INEP(3)->DIEPTSIZ == 0U)
    && (pdev->dev_state == USBD_STATE_CONFIGURED) ) {
    USBD_LL_Transmit(&hUsbDeviceHS, Custom_notif_EPIN_ADDR,
                     (uint8_t *) notifBuffer, Custom_notif_EPIN_SIZE);
  } else {
    /* If we got here, we aren't ready to send data */
    return;
  }
  
  // HAL_Delay(20);

  /* Reset */
  doneI = false;
  doneQ = false;
  memset(&stats, 0, sizeof(stats));

  /* Set how many buffer writes to USB should happen */
  bufferCount = (samples*chirps)/BUFFSIZE;

    /* Enable the DMA request generation */
  __HAL_TIM_ENABLE_DMA(&htim1, TIM_DMA_CC1);
  __HAL_TIM_ENABLE_DMA(&htim1, TIM_DMA_CC2);

  /* Start clocking the ADC and sending to USB */
  /* This one triggers DMA I, and the output clock pin */
  if (HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_1))
    Error_Handler();
  /* This one triggers DMA Q */
  if (HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_2))
    Error_Handler();
  
  while (!doneI || !doneQ) {
    /* Wait here until we're done, unless we fail too hard */
    __NOP();
    if ((stats.i0miss+stats.i1miss > THRESH)||(stats.q0miss+stats.q1miss > THRESH))
      break;
  }

  /* Stop the things */
  if (HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1))
    Error_Handler();
  if (HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2))
    Error_Handler();
  __HAL_TIM_DISABLE_DMA(&htim1, TIM_DMA_CC1);
  __HAL_TIM_DISABLE_DMA(&htim1, TIM_DMA_CC2);

  /* Send unsent data */
  uint32_t bytesUnsent = 2 * ((samples*chirps) - (bufferCount*BUFFSIZE));
  if (bytesUnsent) {
    uint8_t* buf;
    if (htim1.hdma[TIM_DMA_ID_CC1]->Instance->CR & DMA_SxCR_CT) {
      buf = (uint8_t *) buffer_I1;
    } else {
      buf = (uint8_t *) buffer_I0;
    }
    USBD_LL_Transmit(&hUsbDeviceHS, Custom_ADC_I_EPIN_ADDR, buf, bytesUnsent);
    if (htim1.hdma[TIM_DMA_ID_CC2]->Instance->CR & DMA_SxCR_CT) {
      buf = (uint8_t *) buffer_Q1;
    } else {
      buf = (uint8_t *) buffer_Q0;
    }
    USBD_LL_Transmit(&hUsbDeviceHS, Custom_ADC_Q_EPIN_ADDR, buf, bytesUnsent);
  }
}

/**
 * @brief I channel memory 0 transfer complete callback
 * 
 * @param hdma 
 */
void Protocol_sendI0(struct __DMA_HandleTypeDef * hdma) {
  (void) hdma;
  if (!Protocol_chirpIDone()) {
    if (USBx_INEP(1)->DIEPTSIZ == 0U) {
      // ((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state = Custom_BUSY;
      USBD_LL_Transmit(&hUsbDeviceHS, Custom_ADC_I_EPIN_ADDR, (uint8_t*) buffer_I0, 2*BUFFSIZE);
      stats.i0++;
    } else {
      stats.i0miss++;
    }
  } else {
    doneI = true;
  }
}

/**
 * @brief I channel memory 1 transfer complete callback
 * 
 * @param hdma 
 */
void Protocol_sendI1(struct __DMA_HandleTypeDef * hdma) {
  (void) hdma;
  if (!Protocol_chirpIDone()) {
    if (USBx_INEP(2)->DIEPTSIZ == 0U) {
      // ((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state = Custom_BUSY;
      USBD_LL_Transmit(&hUsbDeviceHS, Custom_ADC_I_EPIN_ADDR, (uint8_t*) buffer_I1, 2*BUFFSIZE);
      stats.i1++;
    } else {
      stats.i1miss++;
    }
  } else {
    doneI = true;
  }
}

/**
 * @brief Q channel memory 0 transfer complete callback
 * 
 * @param hdma 
 */
void Protocol_sendQ0(struct __DMA_HandleTypeDef * hdma) {
  (void) hdma;
  if (!Protocol_chirpQDone()) {
    if (USBx_INEP(2)->DIEPTSIZ == 0U) {
      // ((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state = Custom_BUSY;
      USBD_LL_Transmit(&hUsbDeviceHS, Custom_ADC_Q_EPIN_ADDR, (uint8_t*) buffer_Q0, 2*BUFFSIZE);
      stats.q0++;
    } else {
      stats.q0miss++;
    }
  } else {
    doneQ = true;
  }
}

/**
 * @brief Q channel memory 1 transfer complete callback
 * 
 * @param hdma 
 */
void Protocol_sendQ1(struct __DMA_HandleTypeDef * hdma) {
  (void) hdma;
  if (!Protocol_chirpQDone()) {
    if (/*(((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state == Custom_IDLE)
      && */((USBx_INEP(2)->DIEPTSIZ == 0U))) {
      // ((USBD_Custom_HandleTypeDef *)pdev->pClassData)->state = Custom_BUSY;
      USBD_LL_Transmit(&hUsbDeviceHS, Custom_ADC_Q_EPIN_ADDR, (uint8_t*) buffer_Q1, 2*BUFFSIZE);
      stats.q1++;
    } else {
      stats.q1miss++;
    }
  } else {
    doneQ = true;
  }
}

/**
 * @brief Returns true if all I data sent
 * 
 * @return true 
 * @return false 
 */
static bool Protocol_chirpIDone() {
  bool ret = false;
  if (stats.i0+stats.i0miss+stats.i1+stats.i1miss >= bufferCount) {
    ret = true;
  }
  return ret;
}

/**
 * @brief Returns true if all Q data sent
 * 
 * @return true 
 * @return false 
 */
static bool Protocol_chirpQDone() {
  bool ret = false;
  if (stats.q0+stats.q0miss+stats.q1+stats.q1miss >= bufferCount) {
    ret = true;
  }
  return ret;
}

/**
 * @brief Waits for ready packet from PC
 * 
 */
void Protocol_waitForConfig() {
  USBD_LL_PrepareReceive(&hUsbDeviceHS, Custom_config_EPOUT_ADDR, (uint8_t *)configBuffer, Custom_config_EPOUT_SIZE);
  while (USBx_OUTEP(1)->DOEPTSIZ == 0U) {
    __NOP();
  }
  /* Wait for the transfer to happen in its entirety. Maybe useless. */
  HAL_Delay(5);
  sscanf(configBuffer, "confg:0x%08lX;0x%08lX", &samples, &chirps);
}

/**
 * @brief Waits for rising edge on trigger pin
 * 
 */
void Protocol_waitForChirpStart() {
  while (HAL_GPIO_ReadPin(trigger_GPIO_Port, trigger_PIN)) {
    __NOP();
  }
  while (!HAL_GPIO_ReadPin(trigger_GPIO_Port, trigger_PIN)) {
    __NOP();
  }
}
