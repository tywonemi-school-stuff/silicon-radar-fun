#include "renderer.h"
#include <QPainter>
#include <QPaintEvent>
#include <QImage>
#include <QDateTime>
#include <QTransform>
#include <algorithm>
#include <cmath>

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <random>
#include <string>
#include <cstring>
#include <QtCore/QDebug>
#include "constants.h"
#include "units.h"
#include "mainwindow.h"
#include "usb.h"
#include <fftw3.h>
#include <iostream>
#include <fstream>


using namespace units::constants;
using namespace units::dimensionless;
using namespace units::time;
using namespace units::frequency;
using namespace units::length;
using namespace units::velocity;
using namespace units::angular_velocity;
using namespace units::angle;

template<class IteratorIn, class IteratorOut>
void to_string(IteratorIn first, IteratorIn last, IteratorOut out);
static std::default_random_engine randGen((std::random_device())());

static QTextBrowser* debugText;
static QChartView* chartView;

extern unsigned char turbo_srgb_bytes[256][3];

#define EL(x,y) (y + x * samplesPerChirpValid)

//! [0]
Renderer::Renderer(Config inconfig) : 
    data    (inconfig.M, vector<complex<double>>(inconfig.fs * inconfig.T, 0)),
    xWindow (inconfig.M, 0),
    yWindow (inconfig.fs * inconfig.T, 0)
{
    array    = fftw_alloc_complex(inconfig.M * inconfig.fs * inconfig.T);
    spectrum = fftw_alloc_complex(inconfig.M * inconfig.fs * inconfig.T);
    config = inconfig;
    QLinearGradient gradient(QPointF(50, -20), QPointF(80, 20));
    gradient.setColorAt(0.0, Qt::white);
    gradient.setColorAt(1.0, QColor(0xa6, 0xce, 0x39));

    background = QBrush(QColor(64, 32, 64));
    circleBrush = QBrush(gradient);
    circlePen = QPen(Qt::black);
    circlePen.setWidth(1);
    textPen = QPen(Qt::white);
    textFont.setPixelSize(50);

    replan = true;
    index = 0;
    
    /* Sample count between two chirp starts */
    samplesPerChirpRequested = (int)(config.fs * config.Tc);
    /* Valid chirp samples */
    samplesPerChirpValid = (int)(config.fs * config.T);
    
}
//! [0]

static double average;
//! [1]
void Renderer::paint()
{
    startTimer();

    int xmax = config.M;
    // int xmax = 100;
    int ymax = samplesPerChirpValid;
    // int ymax = 100;
    int low = 0;
    int high = 0;

    QRgb line[xmax];
    QImage image(xmax, ymax, QImage::Format_RGB32);
    image.fill(qRgb(100,100,100));

    vector<vector<double>> absmat(
        X_SIZE, vector<double>(Y_SIZE));
    vector<vector<bool>> targets(
        X_SIZE, vector<bool>(Y_SIZE));
    
    for (int y = 0; y < ymax; y++) {
        for (int x = 0; x < xmax; x++) {
            double abss = 0;
            abss = 1/average*std::abs(data[x][y]);
//            double argg = std::arg(data[x][y]);
            absmat[x][y] = abss;
           abss = 40*abss;
            // abss = 40*std::log(abss);
            if(abss>255) high++;
            if(abss<1) low++;

            abss = std::min(std::max(abss,0.0),255.0);
            if (std::isnan(abss))
                abss = 0.0;
//            argg = (argg+M_PI)*360/2/M_PI;
//            argg = std::max(std::min(std::floor(argg),360.0),0.0);
            line[x] = qRgb(turbo_srgb_bytes[(int)abss][0], //red
                           turbo_srgb_bytes[(int)abss][1], //green
                           turbo_srgb_bytes[(int)abss][2]);
                           
            image.setPixel(x,y,line[x]);

        }

    }
    
    if (chartView == NULL) {
        debugText->append ("no chart view");
        return;
    }
    /* passChartView */
    QChart* chart = chartView->chart();
    int cWidth = static_cast<int> (chart->plotArea().width());
    int cHeight = static_cast<int>(chart->plotArea().height());
    int ViewW = static_cast<int>  (chartView->width());
    int ViewH = static_cast<int>  (chartView->height());

    /* scale the image to fit plot area */

    QImage img3 = image.scaled(QSize(cWidth, cHeight));
    QImage translated(ViewW, ViewH, QImage::Format_RGB32);
    translated.fill(Qt::white);
    QPainter cPainter(&translated);
    QPointF TopLeft = chart->plotArea().topLeft();
    cPainter.drawImage(TopLeft, img3);

    chart->setPlotAreaBackgroundBrush(translated);
    chart->setPlotAreaBackgroundVisible(true);
    
    // CFAR(absmat, targets, 4, 4, 0.1, 4);

    QDateTime now = QDateTime::currentDateTime();
    
    image.save("rvplot.png", "PNG", 100);
    
    // printTargets(targets, 10);
    return;
}
//! [3]
void Renderer::rippleFill(vector<vector<complex<double>>> &tbf) {
    for (int x = 0; x<(int)tbf.size(); x++) {
        for (int y = 0; y<(int)tbf[0].size(); y++) {
            tbf[x][y] = sin(100*x+200*y);
        }
    }
}
void Renderer::transpose(vector<vector<complex<double>>> &in, vector<vector<complex<double>>> &out) {
    for (int y = 0; y<(int)in[0].size(); y++) {
        vector<complex<double>> temp;
        for (int x = 0; x<(int)in.size(); x++) {
            temp.push_back(in[x][y]);
        }
        out.push_back(temp);
    }
}
double Renderer::avg(vector<vector<complex<double>>> &in) {
    double average = 0.0;
    for (int x = 0; x<(int)in.size(); x++) {
        for (int y = 0; y<(int)in[0].size(); y++) {
            average += std::abs(in[x][y]);
        }
    }
    average = average/(in[0].size()*in.size());
    return average;
}
double Renderer::avg(fftw_complex* in, int xsize, int ysize) {
    double average = 0.0;
    for (int x = 0; x<xsize; x++) {
        for (int y = 0; y<ysize; y++) {
            average += std::abs((complex<double>)in[x][y]);
        }
    }
    average = average/(xsize*ysize);
    return average;
}

void Renderer::radarFill(vector<vector<complex<double>>> &tbf) { //TBD

    startTimer();

    double A = 40; /* amplitude */

    meter_t distance_m(10);
    meters_per_second_t velocity_mps(0);
    // meters_per_second_t velocity_mps(12); /* Usain Bolt */

    second_t Ts_s(1/(5e6));
    hertz_t fs_Hz(5e6); //todo sync with above line
    hertz_t f0_Hz(24e9);
    hertz_t df_Hz(200e6);

    int Ns = samplesPerChirpValid;
    int Nchirp = config.M;

    // auto Tc = Ns*Ts_s; /* chirp time */
    auto Tc = second_t(config.T); /* Delays between chirps ignored for now */
    // auto alpha = df_Hz/Tc; /* chirp rate */
    auto alpha = scalar_t(config.alfa)*(hertz_t(1)*hertz_t(1));
    hertz_t f_T = scalar_t(2)*alpha*distance_m/c;

    /* c is speed of light */
    for (int k = 0; k < Nchirp; k++) {
        for (int i = 0; i < Ns; i++) {

            scalar_t _k = scalar_t(k);
            scalar_t _i = scalar_t(i);

            auto theta = scalar_t(4*M_PI)*(f0_Hz*(distance_m+velocity_mps*_k*Tc))/(c);

            complex<double> inner = complex<double>(0,(scalar_t(2*M_PI)*f_T*_i*Ts_s + theta).value());
            tbf[k][i] += A * exp(inner);
        }
    }
    vmax = (c*(fs_Hz/f0_Hz)).value();
    rmax = (c*(fs_Hz/(4*alpha))).value();
    
    endTimer("Data gen");
}

void Renderer::passChartView(void* in_view) {
    chartView = (QChartView*)in_view;
}

void Renderer::passTextBrowser(QTextBrowser* in_textb) {
    debugText = in_textb;
}

void Renderer::recalculate(uint8_t* buffer_I, uint8_t* buffer_Q, int channel) {
    if ((buffer_I == NULL )||(buffer_Q == NULL)) {
        /* Received data */
        radarFill(data);
    } else {
        /* No data */
        /* Only run once called from each channel callbacks */
        gotChannel[channel] = true;
        if (!gotChannel[0] || !gotChannel[1])
            return;
        buildComplex(data, (uint16_t*) buffer_I, (uint16_t*) buffer_Q, config.M, samplesPerChirpValid, samplesPerChirpRequested);
    }
    gotChannel[0] = false;
    gotChannel[1] = false;
    vector<vector<complex<double>>> temp;

    /* FFT setup, runs only once */
    startTimer();
    if (replan) {
        fftPlan = fftw_plan_dft_2d(config.M, samplesPerChirpValid, array, spectrum, FFTW_FORWARD, FFTW_MEASURE);
        /* Max even if we have less data */
        replan = false;
        VecBuildBlackmanHarrisWindow( &xWindow.front(), config.M );
        VecBuildBlackmanHarrisWindow( &yWindow.front(), samplesPerChirpValid );
        endTimer("FFTW planning");

        startTimer();
    }

    /* Apply window */
    for(int x = 0; x < config.M; x++) {
        for(int y = 0; y < samplesPerChirpValid; y++) {
            array[EL(x,y)][0] = data[x][y].real() * xWindow[x] * yWindow[y];
            array[EL(x,y)][1] = data[x][y].imag() * xWindow[x] * yWindow[y];
        }
    }
    endTimer("Windowing");

    /* Execute FFT */
    startTimer();
    fftw_execute(fftPlan);
    endTimer("FFTW execution");
    
    /* Copy from the FFTW contiguous array */
    startTimer();
    for(int y = 0; y < samplesPerChirpValid; y++) {
        for(int x = 0; x < config.M; x++) {
            data[x][y] = complex<double>(spectrum[EL(x,y)][0],spectrum[EL(x,y)][1]);
        }
    }
    /* Get average absolute value for comfy renders */
    pythonPrintArr();
    pythonPrintMat();
    average = avg(data);
    endTimer("Recalc post");
    paint();
}

/* Complex matrix printer for Python verification */
void Renderer::pythonPrintMat() {
    std::string numpy;
    numpy += "[";
    for(int x = 0; x < config.M; x++) {
        numpy += "[";
        for(int y = 0; y < samplesPerChirpValid; y++) {
            numpy += std::to_string(data[x][y].real());
            numpy += "+";
            numpy += std::to_string(data[x][y].imag());
            numpy += "j,";
        }
        numpy += "],";
    }
    numpy += "]";
    std::ofstream myfile;
    myfile.open ("numpy.txt");
    myfile << numpy;
    myfile.close();
}

/* Complex array printer for Python verification
    prints only zero-velocity and one tenth of the max distance */
void Renderer::pythonPrintArr() {
    std::string numpy;
    numpy += "[";
    for(int y = 0; y < (samplesPerChirpValid/10); y++) {
        numpy += std::to_string(data[0][y].real());
        numpy += "+";
        numpy += std::to_string(data[0][y].imag());
        numpy += "j,";
    }
    numpy += "]";
    std::ofstream myfile;
    myfile.open ("constant.txt");
    myfile << numpy;
    myfile.close();
}

void Renderer::startTimer() {
    timer.start();
}

void Renderer::endTimer(QString actionName) {
    long long unsigned int nanos = timer.nsecsElapsed();
    QString str;
    str.setNum(nanos/1000);
    debugText->append(actionName + " took: " + str + " μs");
}

/* Build fourth order Blackmann-Harris sampled window function */
/* From https://stackoverflow.com/a/10663313/8048373 */
bool Renderer::VecBuildBlackmanHarrisWindow( double* pOut, unsigned int num )
{
    const double a0      = 0.35875f;
    const double a1      = 0.48829f;
    const double a2      = 0.14128f;
    const double a3      = 0.01168f;

    unsigned int idx    = 0;
    while( idx < num )
    {
        pOut[idx]   = a0 - (a1 * cosf( (2.0f * M_PI * idx) / (num - 1) )) + (a2 * cosf( (4.0f * M_PI * idx) / (num - 1) )) - (a3 * cosf( (6.0f * M_PI * idx) / (num - 1) ));
        idx++;
    }
    return true;
}

/* CFAR from https://github.com/ayushishakya/CFARnKF/blob/master/src/detect.cpp */
/* Just removed the ROS and openCV guts and ported to double */
void Renderer::CFAR(vector<vector<double>> &img, vector<vector<bool>> &out, int num_train, int num_guard, float rate1, int block_size)
{
    unsigned long long num_rows = out.size() - (out.size() % num_train);
    unsigned long long num_cols = out[0].size() - (out[0].size() % num_train);
    unsigned long long num_side = num_train / 2;

    double alpha1 = pow(rate1, -1.00 / num_train) - 1;

    for (unsigned long long i = num_side; i <= num_rows; i += block_size)
        for (unsigned long long j = num_side; j <= num_cols; j += block_size)
        {

            double sum1 = 0, sum2 = 0;
            double thresh, p_noise;

            for (unsigned long long x = i - (num_guard / 2); x <= i + (num_guard / 2) + 1; x++) {
                for (unsigned long long y = j - (num_guard / 2); y <= j + (num_guard / 2) + 1; y++) {
                    if ((x >= out.size()) || (y >= out[0].size())) {
                        continue;
                    }
                    sum1 += img[x][y];
                }
            }
            for (unsigned long long x = i - num_side; x <= i + num_side + 1; x++) {
                for (unsigned long long y = j - num_side; y <= j + num_side + 1; y++) {
                    if ((x >= out.size()) || (y >= out[0].size())) {
                        continue;
                    }
                    sum2 += img[x][y];
                }
            }
            p_noise = fabs(sum1 - sum2) / num_train;
            thresh = alpha1 * p_noise;

            if (img[i][j] > thresh)
            {
                for (unsigned long long k = i - block_size / 2; (k <= i + block_size / 2) && (k < num_rows); k++) {
                    for (unsigned long long l = j - block_size / 2; (l <= j + block_size / 2) && (l < num_cols); l++) {
                        if ((k >= out.size()) || (l >= out[0].size())) {
                            continue;
                        }
                        out[k][l] = true;
                    }
                }
            }
        }
}

void Renderer::printTargets(vector<vector<bool>> &targets, int max)
{
    QString str, tmp;
    int count = 0;
    str += "Targets: {";
    for (unsigned long i = 0; i < targets.size(); i++) {
        for (unsigned long j = 0; j < targets[0].size(); j++) {
            if (targets[i][j]) {
                str += "(";
                tmp.setNum(i);
                str += tmp;
                str += ",";
                tmp.setNum(j);
                str += tmp;
                str += ")\n";
                count ++;
                if (count > max) {
                    str += "}";
                    debugText->append(str);
                    return;
                }
            }
        }
    }
    str += "}";
    debugText->append(str);
}

/* Builds a complex array from data received.
TODO: these comments are outdated, the array is transposed
Example for xvalid=4, xsize=6, ysize=3:

         xsize       xsize       xsize
         <---------> <---------> <---------> 
         xvalid      xvalid      xvalid
         <----->     <----->     <-----> 
buffer = 1 2 3 4 0 0 5 6 7 8 0 0 9 A B C 0 0

      xvalid
      <----->
      1 2 3 4  ^
out = 5 6 7 8  | ysize
      9 A B C  v
*/

void Renderer::buildComplex(vector<vector<complex<double>>> &out, uint16_t* buffer_I, uint16_t* buffer_Q, int xsize, int yvalid, int ysize) {
    for (int j = 0; j < yvalid; j++) {
        for (int i = 0; i < xsize; i++) {
            out[i][j] = complex<double>((double)buffer_I[j + ysize*i], (double)buffer_Q[j + ysize*i]);
        }
    }
}

void Renderer::resetState() {
    gotChannel[0] = false;
    gotChannel[1] = false;
}
