/* From https://dspace.cvut.cz/handle/10467/83303 */
#ifndef USBHANDLER_H
#define USBHANDLER_H

#include <QThread>
#include "libusb.h"

class USBHandler : public QThread {
  Q_OBJECT
public:
  USBHandler() { this->running = false; }
  volatile bool running; // event handler running

protected:
  void run(); // run thread
};

#endif // LIBUSBEVENTHANDLER_H
