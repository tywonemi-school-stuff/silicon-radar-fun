#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "config.h"
#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>
#include <string>
#include <QtCore/QDebug>
#include <QThread>
#include <QPushButton>
#include <QSettings>

#include "glwidget.h"
// plotting
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include "rv_chart.h"

using std::complex;
using std::cout;
using std::endl;
using std::vector;

template<class IteratorIn, class IteratorOut>
void to_string(IteratorIn first, IteratorIn last, IteratorOut out);
std::default_random_engine randGen((std::random_device())());

MainWindow::MainWindow(QString configfile, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    QString configpath = QApplication::applicationDirPath() + "/config/" + configfile;
    QSettings settings(configpath , QSettings::IniFormat);
    Config config;
    config.fs    = settings.value("sampler/fs", "0").toString().toDouble();
    QString mode = settings.value("sampler/mode", "upchirp 1CH").toString();
    config.f0    = settings.value("radar/f0", "0").toString().toDouble();
    config.T     = settings.value("radar/T", "0").toString().toDouble();
    config.Tc    = settings.value("radar/Tc", "0").toString().toDouble();
    config.alfa  = settings.value("radar/alfa", "0").toString().toDouble();
    config.M     = settings.value("radar/M", "0").toString().toInt();

    ui->setupUi(this);
    
    auto horiChart = new QChart();
    horiChart->legend()->hide();
    QLineSeries *series = new QLineSeries;

    horiChart->addSeries(series);
    horiChart->createDefaultAxes();

    horiChart->axes(Qt::Vertical).first()->setGridLineVisible(false);
    horiChart->axes(Qt::Horizontal).first()->setGridLineVisible(false);
    double c = 2.998e8; /* Speed of light */
    horiChart->axes(Qt::Vertical).first()->setMax(c/(2*config.f0*config.Tc));
    horiChart->axes(Qt::Horizontal).first()->setMax((c*5e6)/(2*config.alfa));

    ui->horizontalAxis->setChart(horiChart);
    // QValueAxis *axisX = new QValueAxis;

    // axisX->setRange(10, 20.5);
    // axisX->setTickCount(50);
    // axisX->setLabelFormat("%.2f");
    // axisX->alignment() = Qt::AlignTop;

    ui->textBrowser->clear();

    auto rvChart = new rvGLWidget(config);
    rvChart->passTextBrowser(ui->textBrowser);
    rvChart->passChartView(ui->horizontalAxis);
    rvChart->recalculate(NULL, NULL, 0);
    show();
    USBConnection* usb = new USBConnection( config);
    usb->passTextBrowser(ui->textBrowser);
    connect(usb, &USBConnection::dataStale, rvChart, &rvGLWidget::resetState);
    connect(usb, &USBConnection::newData, rvChart, &rvGLWidget::recalculate);
    connect(ui->pushButton, &QPushButton::clicked, usb, &USBConnection::reconnect);

    usb->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/** some debug printy functions */
template<class IteratorIn, class IteratorOut>
void to_string(IteratorIn first, IteratorIn last, IteratorOut out)
{
    std::transform(first, last, out,
                   [](typename std::iterator_traits<IteratorIn>::value_type d) { return std::to_string(d); } );
}
