#include "libusb.h"
#include "usb.h"

#include <QThread>
#include <QtDebug>
#include <string>
#include <cstring>
#include <cstdio>

/* code inspired by the example at https://github.com/libusb/libusb/blob/master/examples/dpfp.c */
using namespace std;
USBConnection::USBConnection(Config inconfig) {
    this->eventHandler = new USBHandler();
    this->ITransfer = NULL;
    this->QTransfer = NULL;
    this->config = inconfig;
    /* Sample count between two chirp starts */
    samplesPerChirpConfigure = (int)(config.fs * config.Tc);

    // QString configMessage = "confg:" + QString::number(N) + ";" + QString::number(config.M);
    /* Set how many to request */
    snprintf(configBuffer, sizeof(configBuffer), "confg:0x%08lX;0x%08lX", samplesPerChirpConfigure, config.M);
}

void USBConnection::run() {
    QThread::msleep(10);

    int r = 1;
//connected
    libusb_context* ctx;
    r = libusb_init(&ctx);
    if (r < 0) {
        debugText->append("failed to initialise libusb\n");
        return;
    }
    debugText->append("libusb version:");
    auto ver = libusb_get_version();
    std::string prstr;
    prstr +=std::to_string(ver->major)+=".";
    prstr +=std::to_string(ver->minor)+=".";
    prstr +=std::to_string(ver->micro)+=".";
    prstr +=std::to_string(ver->nano)+="";
    debugText->append(QString::fromStdString(prstr));
    reconnect();
    libusb_set_option(ctx, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_WARNING);

}


int USBConnection::find_device(void)
{
    libusb_device **devs;
    libusb_device *dev;

    int cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0){
        libusb_exit(NULL);
        return (int) cnt;
    }

    int ret = -69;
    int i = 0;
    devh = NULL;
    while ((dev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0) {
            debugText->append("failed to get device descriptor");
            return -EIO;
        }
//        if (desc.idVendor == 0x0FEE &&desc.idProduct == 0xF055) { /* USBHidTest */
        if (desc.idVendor == 1155 && desc.idProduct == 22352) { /* Final board */
            debugText->append("VID,PID match");
            unsigned char str [128];
            ret = 0;
            ret = libusb_open(dev,&devh);
            if (ret) {
                debugText->append("Failed to open");
                debugText->append( libusb_strerror((enum libusb_error )ret) );
                continue;
            }
            libusb_get_string_descriptor_ascii(devh,2,&str[0],128);
            const char* out = (char*) &str[0];
            debugText->append("VID, PID match:");
            debugText->append(out);
            string cppstr(out);
            size_t pos = cppstr.find("SiRad ADC board");
            if (pos != string::npos) { /* If we found a match */
                debugText->append("Device match");
                foundBoard = true;
            }
            if (ret) {
                debugText->append( libusb_strerror((enum libusb_error )ret) );
            }
            break;
        }
    }
    libusb_free_device_list(devs,0);
    if (ret!=0) {
        debugText->append( libusb_strerror((enum libusb_error )ret) );
    } else {
        if (!foundBoard) {
            debugText->append("Did not find device. Check drivers in Device Manager");
        }
    }
    return devh ? 0 : -EIO;
}

void USBConnection::passTextBrowser(QTextBrowser* in_textb) {
    debugText = in_textb;
}

void USBConnection::reconnect() {
    debugText->append("Reconnecting");
    int r = 1;
    r = find_device();
    if (r < 0) {
        debugText->append("Could not find/open device");
        return;
    }

  // detach kernel drivers
    r = libusb_kernel_driver_active(devh, 0);
    if (r == 1) {
        r = libusb_detach_kernel_driver(devh, 0);
        if (r != 0) {
            devh = NULL;
            debugText->append("Detach kernel driver error:");
            debugText->append(libusb_strerror((enum libusb_error )r));
            return;
        }
    }

    r = libusb_claim_interface(devh, 0);
    if (r < 0) {
        debugText->append( "usb_claim_interface error");
        return;
    }
    debugText->append("Claimed interface");
    // start event handling libusb thread
    notifTransfer = libusb_alloc_transfer(0);
    configTransfer = libusb_alloc_transfer(0);
    ITransfer = libusb_alloc_transfer(0);
    QTransfer = libusb_alloc_transfer(0);
//    return;

    this->eventHandler->running = true;
    this->eventHandler->start();

    /* Set up the notification transfer with zero iso packets */
    libusb_fill_bulk_transfer(
        notifTransfer,
        devh,
        notif_EPIN_ADDR,
        (unsigned char *) notifBuffer,
        notif_EPIN_SIZE,
        handleNotification,
        this,
        2000);
    libusb_submit_transfer(notifTransfer);
    
    libusb_fill_bulk_transfer(
        configTransfer,
        devh,
        config_EPOUT_ADDR,
        (unsigned char *) configBuffer,
        config_EPOUT_SIZE,
        handleNotification,
        this,
        2000);
    libusb_submit_transfer(configTransfer);
    debugText->append("Submitted");
}

void USBConnection::clearStats() {
    total = 0;
    dropped = 0;
    elapsedI = INT_MAX;
    elapsedQ = INT_MAX;
    timer.start();
}

void USBConnection::endTimerI() {
    elapsedI = timer.nsecsElapsed();
}

void USBConnection::endTimerQ() {
    elapsedQ = timer.nsecsElapsed();
}

void USBConnection::printStats() {
    int elapsed = std::max(elapsedI, elapsedQ);
    QString strBytes;
    strBytes.setNum(dropped);
    debugText->append("Dropped: " + strBytes + " packets");
    strBytes.setNum(((float)total*8/((float)elapsed/1000))/1024/1024*1000*1000);
    debugText->append("Throughput: " + strBytes + " Mbps");
}

void LIBUSB_CALL USBConnection::handleNotification(struct libusb_transfer *transfer) {
    USBConnection* instance = reinterpret_cast<USBConnection *>(transfer->user_data);
    if (transfer->actual_length > 0) {
        std::string received(instance->notifBuffer);
        std::size_t found = received.find("chirp:");
        int ret;
        if (found!=std::string::npos) {
            /* Get expected number of chirp samples */
            std::string hexSamps  = received.substr(6,18);
            std::string hexChirps = received.substr(19,31);
            instance->samplesPerChirpToReceive = std::stoi(hexSamps,0,16);
            instance->chirpsExpected  = std::stoi(hexChirps,0,16);
            if (instance->chirpsExpected != instance->config.M) {
                instance->debugText->append("Warning: Chirp count mismatch!");
            }
            if (instance->samplesPerChirpToReceive > CHIRP_SAMPLES_MAX) {
                instance->samplesPerChirpToReceive = CHIRP_SAMPLES_MAX;
                instance->debugText->append("Chirp too big");
            } 
            if ((!instance->lockI) && (!instance->lockQ)) {
                instance->lockI = true;
                instance->lockQ = true;
                libusb_fill_bulk_transfer(instance->ITransfer,
                    instance->devh, ADC_I_EPIN_ADDR, instance->IBuffer,
                    2*instance->samplesPerChirpToReceive*instance->chirpsExpected,
                    instance->handleI, transfer->user_data,
                    instance->timeout);
                libusb_fill_bulk_transfer(instance->QTransfer,
                    instance->devh, ADC_Q_EPIN_ADDR, instance->QBuffer,
                    2*instance->samplesPerChirpToReceive*instance->chirpsExpected,
                    instance->handleQ, transfer->user_data,
                    instance->timeout);

                ret = libusb_submit_transfer(instance->ITransfer);
                ret = libusb_submit_transfer(instance->QTransfer);
                instance->total = instance->bytesI + instance->bytesQ;
                instance->bytesI = 0;
                instance->bytesQ = 0;
                instance->printStats();
                instance->clearStats();
            }
        }
    }
    libusb_submit_transfer(reinterpret_cast<USBConnection *>(transfer->user_data)->notifTransfer); /* Resubmit */
}

void LIBUSB_CALL USBConnection::handleI(struct libusb_transfer *transfer) {
    USBConnection* instance = reinterpret_cast<USBConnection *>(transfer->user_data);
    instance->endTimerI();
    instance->lockI = false;
    if (transfer->status == LIBUSB_TRANSFER_STALL) {
        libusb_clear_halt(instance->devh, ADC_I_EPIN_ADDR);
        instance->debugText->append("I stalled");
        return;
    }
    instance->dropped += transfer->length - transfer->actual_length;
    if (transfer->actual_length > 0) {
        instance->bytesI = transfer->actual_length;
    }
    if (instance->bytesI == 2*instance->samplesPerChirpToReceive*instance->chirpsExpected) {
        /* Tell the renderer there's new data to work with */
        instance->newData(&(instance->IBuffer[0]), &(instance->QBuffer[0]), 0);
    }
}

void LIBUSB_CALL USBConnection::handleQ(struct libusb_transfer *transfer) {
    USBConnection* instance = reinterpret_cast<USBConnection *>(transfer->user_data);
    instance->endTimerQ();
    instance->lockQ = false;
    if (transfer->status == LIBUSB_TRANSFER_STALL) {
        libusb_clear_halt(instance->devh, ADC_Q_EPIN_ADDR);
        instance->debugText->append("Q stalled");
        return;
    }
    instance->dropped += transfer->length - transfer->actual_length;
    if (transfer->actual_length > 0) {
        instance->bytesQ = transfer->actual_length;
    }
    if (instance->bytesQ == 2*instance->samplesPerChirpToReceive*instance->chirpsExpected) {
        /* Tell the renderer there's new data to work with */
        instance->newData(&(instance->IBuffer[0]), &(instance->QBuffer[0]), 1);
    }
}
