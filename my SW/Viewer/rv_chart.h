#ifndef RV_CHART_H
#define RV_CHART_H

#include <QtCharts/QChartGlobal>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGraphicsWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGraphicsGridLayout>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtCharts/QLineSeries>

QT_CHARTS_USE_NAMESPACE

class rv_chart : public QWidget
{
    Q_OBJECT
public:
    explicit rv_chart(QWidget *parent = nullptr);
private:
    QChart *m_chart;
    QList<QLineSeries *> m_series;

    QChartView *m_chartView;
    QGridLayout *m_mainLayout;
    QGridLayout *m_fontLayout;
signals:

};

#endif // RV_CHART_H

