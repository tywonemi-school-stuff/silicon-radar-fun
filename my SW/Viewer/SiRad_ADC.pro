QT       += charts widgets core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += warn_off
# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES += USE_FFTW
QMAKE_CFLAGS        += -m64
LIBS += -lusb-1.0
LIBS += -lfftw3-3 -lfftw3f-3 -lfftw3l-3

# statically linked, because MinGW
LIBS += -static-libstdc++ -static-libgcc -lwinpthread-1

# INCLUDEPATH += 

SOURCES += \
    glwidget.cpp \
    renderer.cpp \
    main.cpp \
    mainwindow.cpp \
    rv_chart.cpp \
    turbo_colormap.c \
    usb.cpp \
    usbhandler.cpp

HEADERS += \
    config.h \
    constants.h \
    glwidget.h \
    renderer.h \
    mainwindow.h \
    rv_chart.h \
    units.h \
    libusb.h \
    usb.h \
    usbhandler.h

RESOURCES += qdarkstyle/style.qrc

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
