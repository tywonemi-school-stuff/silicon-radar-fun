#include "glwidget.h"
#include "renderer.h"

#include <QPainter>
#include <QTimer>
#include <QString>
#include <QMainWindow>

#include "constants.h"

rvGLWidget::rvGLWidget(Config config, QWidget *parent)
    : QOpenGLWidget(parent)
{
    renderer = new Renderer(config);
    elapsed = 0;
    setAutoFillBackground(false);
}

void rvGLWidget::recalculate(uint8_t* buffer_I, uint8_t* buffer_Q, int channel)
{
    renderer->recalculate(buffer_I, buffer_Q, channel);
}
void rvGLWidget::resetState()
{
    renderer->resetState();
}

void rvGLWidget::passTextBrowser(QTextBrowser* in_textb) {
    renderer->passTextBrowser(in_textb);
}
void rvGLWidget::passChartView(void* in_view) {
    renderer->passChartView((void*)in_view);
}
