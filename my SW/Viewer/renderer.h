#ifndef RENDERER_H
#define RENDERER_H

#include <QBrush>
#include <QFont>
#include <QPen>
#include <QWidget>
#include <QTextBrowser>
#include <QElapsedTimer>
#include <QMainWindow>
 #include <QtCharts/QChart>
 #include <QtCharts/QChartView>
#include <fftw3.h>
#include <complex>
#include "constants.h"
#include <string>
#include <vector>
#include "config.h"

using std::vector;
using std::complex;

using namespace QtCharts;

//! [0]
class Renderer
{
public:
    Renderer(Config inconfig);

public:
    void paint();
    void passTextBrowser(QTextBrowser* in_textb);
    void passChartView(void* in_view);
    void recalculate(uint8_t* buffer_I, uint8_t* buffer_Q, int channel);
    void resetState();
    void pythonPrintMat();
    void pythonPrintArr();
    void startTimer();
    void endTimer(QString actionName);
private:
    void radarFill(vector<vector<complex<double>>> &tbf);
    void rippleFill(vector<vector<complex<double>>> &tbf);
    void transpose(vector<vector<complex<double>>> &in, vector<vector<complex<double>>> &out);
    void buildComplex(vector<vector<complex<double>>> &out, uint16_t* buffer_I, uint16_t* buffer_Q, int xvalid, int xsize, int ysize);
    double avg(vector<vector<complex<double>>> &in);
    double avg(fftw_complex* in, int xsize, int ysize);
    bool VecBuildBlackmanHarrisWindow( double* pOut, unsigned int num );
    void CFAR(vector<vector<double>> &img, vector<vector<bool>> &out, int num_train, int num_guard, float rate1, int block_size);
    void printTargets(vector<vector<bool>> &targets, int max);
    Config config;
    QBrush background;
    QBrush circleBrush;
    QFont textFont;
    QPen circlePen;
    QPen textPen;
    QElapsedTimer timer;
    int index;
    bool replan;
    bool gotChannel[2];
    fftw_plan fftPlan;
    fftw_complex* array;
    fftw_complex* spectrum;
    vector<vector<complex<double>>> data;
    vector< double > xWindow;
    vector< double > yWindow;
    double vmax;
    double rmax;
    int samplesPerChirpValid;
    int samplesPerChirpRequested;
};
//! [0]

#endif
