#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QTextBrowser>
#include "config.h"

class Renderer;

class rvGLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    explicit rvGLWidget(Config config, QWidget *parent = nullptr);
    void passTextBrowser(QTextBrowser* in_textb);
    void passChartView(void* in_view);
public slots:
    void resetState();
    void recalculate(uint8_t* buffer_I, uint8_t* buffer_Q, int channel);

private:
    Renderer *renderer;
    int elapsed;
};


#endif
