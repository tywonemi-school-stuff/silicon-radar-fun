#include "mainwindow.h"

#include <QApplication>
#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QSettings>

#include "constants.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString config;
    if (argc < 1) {
        config = "default.ini";
    } else {
        QString str = argv[1];
        config = str;
    }
    MainWindow w(config);
    w.show();

    QFile f(":qdarkstyle/style.qss");

    if (!f.exists()) {
        printf("Unable to set stylesheet, file not found\n");
    } else {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        a.setStyleSheet(ts.readAll());
    }
    return a.exec();
}
