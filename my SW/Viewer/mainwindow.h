#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QWidget>
#include "renderer.h"
#include "usb.h"
#include <QTextBrowser>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QString configfile = QString(":/default.ini"), QWidget *parent = nullptr);
    ~MainWindow();
    QTextBrowser* logBox;
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
