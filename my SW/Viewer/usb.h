#ifndef USB_H
#define USB_H
#include <QObject>
#include <QThread>
#include <QTextBrowser>
#include <QElapsedTimer>
#include <QMainWindow>
#include "usbhandler.h"
#include "libusb.h"

/* These must be synchronized with device firmware, obviously */
#define ADC_I_EPIN_ADDR           0x81U
#define ADC_I_EPIN_SIZE           0x200U
#define ADC_Q_EPIN_ADDR           0x82U
#define ADC_Q_EPIN_SIZE           0x200U
#define notif_EPIN_ADDR           0x83U
#define notif_EPIN_SIZE           0x200U
#define config_EPOUT_ADDR         0x01U
#define config_EPOUT_SIZE         0x200U

/* how many transfers the buffer lasts */
/* how many packets sent per SW burst */
/* how many bursts are in a full ADC data set */

#include "config.h"
#define CHIRP_SAMPLES_MAX 256*1024

class ADCData;
class USBConnection;

class USBConnection: public QThread
{
//protected:
    Q_OBJECT
    void run() override;

public:
    USBConnection(Config inconfig);
    void passTextBrowser(QTextBrowser* in_textb);
    void clearStats();
    void printStats();

public slots:
    void reconnect();
signals:
    void newData(uint8_t* buffer_I, uint8_t* buffer_Q, int channel);
    void dataStale();

private:
    Config config;
    libusb_transfer* notifTransfer;
    libusb_transfer* configTransfer;
    libusb_transfer* ITransfer;
    libusb_transfer* QTransfer;
    struct libusb_device_handle *devh = NULL;
    static void LIBUSB_CALL handleNotification(struct libusb_transfer *transfer);
    static void LIBUSB_CALL handleI(struct libusb_transfer *transfer);
    static void LIBUSB_CALL handleQ(struct libusb_transfer *transfer);
    USBHandler* eventHandler;
    int find_device();
    int total = 0;
    int dropped = 0;
    int chirpsGot = 0;
    int chirpsExpected = 0;
    int samplesPerChirpToReceive = 0;
    int samplesPerChirpConfigure = 0;
    const int timeout = 150;
    int elapsedI;
    int elapsedQ;
    QElapsedTimer timer;
    void endTimerI();
    void endTimerQ();
    uint8_t IBuffer[2*CHIRP_SAMPLES_MAX];
    uint8_t QBuffer[2*CHIRP_SAMPLES_MAX];
    QTextBrowser* debugText;
    char notifBuffer[notif_EPIN_SIZE] = "";
    char configBuffer[config_EPOUT_SIZE] = "";
    bool foundBoard = false;
    bool lockI = false;
    bool lockQ = false;
    int bytesQ;
    int bytesI;
};

#endif // USB_H
