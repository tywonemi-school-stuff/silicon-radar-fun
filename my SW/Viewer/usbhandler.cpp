/* From https://dspace.cvut.cz/handle/10467/83303 */
#include "usbhandler.h"
#include <QtCore/QDebug>

void USBHandler::run() {
  while (running) {
    libusb_handle_events(nullptr);
    this->usleep(1000);
    // qDebug() << "run it run it";
  }
}
