# Operational Amplifier notes

Turns out my initially selected LMH6628MAX amps aren't rail to rail, and would be cutting into the 3.3V Vin range. To fix this, I have selected this guy:
<https://www.ti.com/lit/ds/symlink/tlv3542.pdf?HQS=TI-null-null-mousermode-df-pf-null-wwe&ts=1593706380786&ref_url=https%253A%252F%252Fcz.mouser.com%252F>

TLV3542 has a similar GBW product, is r2r, and has superior THD scores.

This is going to be the best audio card for bats, ever.