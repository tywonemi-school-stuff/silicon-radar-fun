# USB Notes
Host requests data from device bulk IN endpoint. If the device is unable to provide the data, NAK (no acknowledge) is returned
Default IRQ handler auto-clears only OUT NAK interrupt flags, not IN. Why?
I am getting a lot of OUT setup packet interrupts. Should I be doing things in callback? It seems to be clearing STALLs. Good
Relevant interrupts that I am getting on the fat_IN endpoint:
* ITTXFE IN token received when TxFIFO is empty
* TXFE TxFIFO is half empty
* NAK 
on control INEP0:
ITTXFE, INEPNE IN endpoint NAK effective, TXFE, NAK. This means that INEP0 is disabled, what? why?
SNAK in IEPCTLx requests endpoint to be stopped. INEPNE in IEPINT is set once endpoint has been stopped.
Is my INEP1 size reg at least ok? Yes, XFRSIZ is 0x200, and PKTCNT is 0x1. MCNT is 0 - seems 0 is UB. Watch out,
MCNT is called MULCNT in C defines! It is set only on isochronous. MCNT is only for periodic IN so it's ok

How to capture USB request blocks: <https://wiki.wireshark.org/CaptureSetup/USB>
This does not capture the bus, it captures communication between kernel and controllers. So a response that says "CANCELED" on time out does not mean the device sent anything.

Note: DMA serves periodic first, and in a fixed microframe portion which is set in PERSCHIVL register.
DMA is HS only?

DIEPDMAx DMAADDR is incremented on every AHB transaction - but under what circumstances? 
AHBERR generated when AHB r/w fails in internal DMA mode

Right now: I am getting 5120B 1000x a second is 5120kB/s = 40Mbps?

0x200 RX

In function HAL_PCDEx_SetTxFiFo: "When DMA is used 3n * FIFO locations should be reserved for internal DMA registers". Was that my problem earlier?

We will be getting 64kSamples*4B = 256kB per frame